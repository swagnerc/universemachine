#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <unistd.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"

struct mcmc_data *bestfit, *up, *dn, *median, *up2=NULL, *dn2=NULL;

void gen_observations(char *filename);
void gen_smhm_plots(void);
void gen_wl_plots(void);
void gen_corr_plots(void);
void gen_smfs(void);
void gen_hmfs(void);
void gen_uvlfs(void);
void gen_sfh_data(void);
void gen_infall_stats(void);
void gen_csfrs(void);

#define numbered_fprintf(x, ...) { char _fprintf_buffer[2048]; snprintf(_fprintf_buffer, 2048, __VA_ARGS__); _numbered_fprintf(x, _fprintf_buffer); }
void _numbered_fprintf(FILE *out, char *buffer) {
  char *token=buffer;
  char *token2=buffer;
  strsep(&token, "\n"); 
  int64_t c=0;
  while ((token = strsep(&token2, " "))) {
    fprintf(out, "%s(%"PRId64") ", token, c);
    c++;
  }
  fprintf(out, "\n");
}

int main(int argc, char **argv) {
  //int64_t i,j, length;
  //char subtypes[4] = {'a', 's', 'q', 'c'};

  if (argc < 2) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  r250_init(87L);
  init_cosmology(Om, Ol, h0);
  init_time_table(Om, h0);

  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] POSTPROCESSING_MODE must be enabled to generate plot data.\n");
    exit(EXIT_FAILURE);
  }
  
  char buffer[2048];
  snprintf(buffer, 2048, "%s/post_bestfit.dat", OUTBASE);
  bestfit = load_mcmc_data(buffer);
  snprintf(buffer, 2048, "%s/post_up.dat", OUTBASE);
  up = load_mcmc_data(buffer);
  snprintf(buffer, 2048, "%s/post_dn.dat", OUTBASE);
  dn = load_mcmc_data(buffer);
  snprintf(buffer, 2048, "%s/post_median.dat", OUTBASE);
  median = load_mcmc_data(buffer);

  snprintf(buffer, 2048, "%s/post_up_2sigma.dat", OUTBASE);
  FILE *tmp = fopen(buffer, "r");
  if (tmp) {
    fclose(tmp);
    up2 = load_mcmc_data(buffer);
    snprintf(buffer, 2048, "%s/post_dn_2sigma.dat", OUTBASE);
    dn2 = load_mcmc_data(buffer);
  }    
  
  chdir(OUTBASE);
  check_mkdir("plots", 0755);
  check_mkdir("plots/data", 0755);
  printf("Generating plot data in %s/%s.\n", OUTBASE, "plots/data");
  
  gen_observations("plots/data/obs.txt");
  gen_csfrs();
  gen_smfs();
  gen_hmfs();
  gen_uvlfs();
  gen_smhm_plots();
  gen_wl_plots();
  gen_corr_plots();
  gen_sfh_data();
  gen_infall_stats();
  return 0;
}


void print_model(FILE *out) {
  fprintf(out, "#Model:");
  int64_t i;
  for (i=0; i<NUM_PARAMS; i++)
    fprintf(out, " %.12f", bestfit->model->params[i]);
  fprintf(out, " 0 %.12f\n", CHI2(bestfit->model[0])); 
}


char *obs_data_types[NUM_TYPES] = {"smf", "uvlf", "qf", "qf11", "qf_uvj", "ssfr", "csfr", "csfr_(uv)", "correlation", "cross_correlation", "conformity", "lensing", "cdens_fsf", "cdens_ssfr_sf", "uvsm", "irx"};
char subtypes[4] = {'a', 's', 'q', 'c'};

void gen_observations(char *filename) {
  int64_t i;
  FILE *out = check_fopen(filename, "w");
  print_model(out);
  
  fprintf(out, "#Type Subtype Z1 Z2 step1 step2 SM1 SM2 smb1 smb2 R1 R2 Val Err_h Err_l Model_Val MV+ MV- Chi2 Chi2+ Chi2-\n");
  for (i=0; i<bestfit->num_obs; i++) {
    struct observation *o = bestfit->obs+i;
    struct observation *u = up->obs+i;
    struct observation *d = dn->obs+i;
    fprintf(out, "%s %c %f %f %"PRId64" %"PRId64" %f %f %"PRId64" %"PRId64" %f %f %e %f %f %e %e %e %e %e %e\n", obs_data_types[o->type], subtypes[(int)o->subtype], o->z_low, o->z_high, o->step1, o->step2, o->sm_low, o->sm_high, o->smb1, o->smb2, o->r_low, o->r_high, o->val, o->err_h, o->err_l, o->model_val, u->model_val - o->model_val, o->model_val - d->model_val, o->chi2, u->chi2 - o->chi2, o->chi2-d->chi2);
  }
  fclose(out);
}

#define TRIVAL(x) bestfit->x, (up->x - bestfit->x), (bestfit->x - dn->x)
#define TRIVALM(x) median->x, (up->x - median->x), (median->x - dn->x)
#define TRIVALB(x) bestfit->x[bin], (up->x[bin] - bestfit->x[bin]), (bestfit->x[bin] - dn->x[bin])
#define TRIVALB2(x) bestfit->x[bin], (up2->x[bin] - bestfit->x[bin]), (bestfit->x[bin] - dn2->x[bin])
#define TRIVALMB(x) median->x[bin], (up->x[bin] - median->x[bin]), (median->x[bin] - dn->x[bin])
#define SCALED_TRIVALB(x,y) bestfit->x[bin]*y, (up->x[bin] - bestfit->x[bin])*y, (bestfit->x[bin] - dn->x[bin])*y
#define SCALED_TRIVALB2(x,y) bestfit->x[bin]*y, (up2->x[bin] - bestfit->x[bin])*y, (bestfit->x[bin] - dn2->x[bin])*y
#define PRINT_PARAMS(x) { FILE *out = check_fopen("plots/data/smhm/params/" #x ".txt", "w");  for (j=0; j<SMHM_FIT_PARAMS; j++) { fprintf(out, "%"PRId64" %e %e %e\n", j, TRIVAL(x[j])); } fprintf(out, "#chi2: %e %e %e\n", TRIVAL(x[j])); fclose(out); }
#define RESIDUAL(x,y) ((bestfit->x[bin] && bestfit->y[bin]) ? (bestfit->x[bin] - bestfit->y[bin]) : 0.0)


void gen_csfrs(void) {
  check_mkdir("plots/data/csfrs", 0755);
  int64_t i;
  FILE *out = check_fopen("plots/data/csfrs/csfrs.dat", "w");
  fprintf(out, "#Total_Obs_CSFR: total observed cosmic star formation rate (Msun/yr/Mpc^3)\n");
  fprintf(out, "#Total_Obs_CSFR_UV: total observed cosmic star formation rate (Msun/yr/Mpc^3) for galaxies with M_1500 < %g\n", UV_MAG_THRESH);
  fprintf(out, "#True_CSFR: total true cosmic star formation rate (Msun/yr/Mpc^3)\n");
  fprintf(out, "#Scale Total_Obs_CSFR Err+ Err- Total_Obs_CSFR_UV Err+ Err- True_CSFR Err+ Err- Total_Obs_CSFR_UV/Total_Obs_CSFR Err+ Err-\n");
  double conv_factor = pow(BOX_SIZE/h0, -3);
  for (i=0; i<bestfit->num_scales; i++) {
    int64_t bin = i;
    fprintf(out, "%g %g %g %g %g %g %g %g %g %g %g %g %g\n", bestfit->scales[i],
	    SCALED_TRIVALB(csfr, conv_factor), SCALED_TRIVALB(csfr_uv, conv_factor),
	    SCALED_TRIVALB(true_csfr, conv_factor), TRIVALB(uv_csfr_ratio));
  }
  fclose(out);

  //2sig results
  if (up2 && dn2) {
    out = check_fopen("plots/data/csfrs/csfrs_2sigma.dat", "w");
    fprintf(out, "#Total_Obs_CSFR: total observed cosmic star formation rate (Msun/yr/Mpc^3)\n");
    fprintf(out, "#Total_Obs_CSFR_UV: total observed cosmic star formation rate (Msun/yr/Mpc^3) for galaxies with M_1500 < %g\n", UV_MAG_THRESH);
    fprintf(out, "#True_CSFR: total true cosmic star formation rate (Msun/yr/Mpc^3)\n");
    fprintf(out, "#Scale Total_Obs_CSFR Err+(2sig) Err-(2sig) Total_Obs_CSFR_UV Err+(2sig) Err-(2sig) True_CSFR Err+(2sig) Err-(2sig) Total_Obs_CSFR_UV/Total_Obs_CSFR Err+(2sig) Err-(2sig)\n");
    conv_factor = pow(BOX_SIZE/h0, -3);
    for (i=0; i<bestfit->num_scales; i++) {
      int64_t bin = i;
      fprintf(out, "%g %g %g %g %g %g %g %g %g %g %g %g %g\n", bestfit->scales[i],
	      SCALED_TRIVALB2(csfr, conv_factor), SCALED_TRIVALB2(csfr_uv, conv_factor),
	      SCALED_TRIVALB2(true_csfr, conv_factor), TRIVALB2(uv_csfr_ratio));
    }
    fclose(out);
  }
}

void gen_smfs(void) {
  char buffer[1024];
  check_mkdir("plots/data/smfs", 0755);
  check_mkdir("plots/data/ssfrs", 0755);
  check_mkdir("plots/data/qfs", 0755);
  check_mkdir("plots/data/rejuvenation", 0755);
  check_mkdir("plots/data/ex_situ", 0755);
  check_mkdir("plots/data/smfs/hires", 0755);
  int64_t i, j, k;
  for (i=0; i<bestfit->num_scales; i++) {
    double conv_factor = pow(BOX_SIZE/h0, -3)*SM_RED_BPDEX;
    snprintf(buffer, 1024, "plots/data/smfs/smf_a%f.dat", bestfit->scales[i]);
    FILE *out_smf = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_a%f.dat", bestfit->scales[i]);
    FILE *out_qf = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/ssfrs/ssfr_a%f.dat", bestfit->scales[i]);
    FILE *out_ssfrs = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_groupstats_a%f.dat", bestfit->scales[i]);
    FILE *out_qfg = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_groupstats_infall_a%f.dat", bestfit->scales[i]);
    FILE *out_qfg_infall = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_quenched_infall_a%f.dat", bestfit->scales[i]);
    FILE *out_q_infall = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/rejuvenation/rejuv_a%f.dat", bestfit->scales[i]);
    FILE *out_rejuv = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/ex_situ/ex_situ_a%f.dat", bestfit->scales[i]);
    FILE *out_ex_situ = check_fopen(buffer, "w");
    
    
    fprintf(out_smf, "#Observed stellar masses in Msun\n");
    fprintf(out_smf, "#Quenched threshold: observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_smf, "#Number densities in comoving Mpc^-3 dex^-1.\n");
    fprintf(out_smf, "#Log10(SM_Center) Number_Density Err+ Err- Satellite_fraction Err+ Err- Q_Number_Density Err+ Err- SM_Left_Edge SM_Right_Edge\n");
    fprintf(out_ssfrs, "#Observed stellar masses in Msun\n");
    fprintf(out_ssfrs, "#Observed SSFRs in yr^-1.\n");
    fprintf(out_ssfrs, "#Bestfit_Galaxy_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_ssfrs, "#Log10(SM) <SFR/SM> Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts\n");
    fprintf(out_qf, "#Observed stellar masses in Msun\n");
    fprintf(out_qf, "#Bestfit_Galaxy_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_qf, "#Log10(SM_Center) FQ(SSFR<PRIMUS_Thresh) Err+ Err- FQ(SSFR<%g) Err+ Err- FQ_UVJ Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg, "#Observed stellar masses in Msun\n");
    fprintf(out_qfg, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_qfg, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_qfg, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_qfg, "#Quenched threshold: observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg, "#Bestfit_X_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_qfg, "#Log10(SM_Center) Median_FQ(Centrals) Err+ Err- Median_FQ(Sats) Err+ Err- Median_FQ(Sats_MW) Err+ Err- Median_FQ(Sats_Group) Err+ Err- Median_FQ(Sats_Cluster) Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts Bestfit_Central_Galaxy_Counts Bestfit_Sat_Galaxy_Counts Bestfit_Sats_MW_Counts Bestfit_Sats_GR_Counts Bestfit_Sats_Cluster_Counts\n");
    fprintf(out_qfg_infall, "#Observed stellar masses in Msun\n");
    fprintf(out_qfg_infall, "#Fractions of *quenched satellites* that were quenched after infall\n");
    fprintf(out_qfg_infall, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_qfg_infall, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_qfg_infall, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_qfg_infall, "#Quenched threshold: observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg_infall, "#Bestfit_X_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_qfg_infall, "#Log10(SM_Center) Median_FQ_infall(Sats_MW) Err+ Err- Median_FQ_infall(Sats_Group) Err+ Err- Median_FQ_infall(Sats_Cluster) Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts Bestfit_Sats_MW_Counts Bestfit_Sats_GR_Counts Bestfit_Sats_Cluster_Counts\n");
    fprintf(out_q_infall, "#Observed stellar masses in Msun\n");
    fprintf(out_q_infall, "#Fractions of satellites that were quenched due to infall (f_q,sat - f_q,cen)\n");
    fprintf(out_q_infall, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_q_infall, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_q_infall, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_q_infall, "#Quenched threshold: observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_q_infall, "#Bestfit_X_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_q_infall, "#Log10(SM_Center) Median_FQ_infall(Sats) Err+ Err- Median_FQ_infall(Sats_MW) Err+ Err- Median_FQ_infall(Sats_Group) Err+ Err- Median_FQ_infall(Sats_Cluster) Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts Bestfit_Satellite_Counts Bestfit_Sats_MW_Counts Bestfit_Sats_GR_Counts Bestfit_Sats_Cluster_Counts\n");
    fprintf(out_rejuv, "#Observed stellar masses in Msun\n");
    fprintf(out_rejuv, "#Rejuvenation definition: at least 300Myr quenched, followed by at least 300Myr of star formation.\n");
    fprintf(out_rejuv, "#Quenched threshold: %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_rejuv, "#Bestfit_Galaxy_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_rejuv, "#Log10(SM) F_rejuv Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts\n");
    fprintf(out_ex_situ, "#Observed stellar masses in Msun\n");
    fprintf(out_ex_situ, "#Average fraction of mass accreted from galaxy mergers as opposed to formed in situ.\n");
    fprintf(out_ex_situ, "#Bestfit_Galaxy_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_ex_situ, "#Log10(SM) F_ex_situ Err+ Err- SM_Left_Edge SM_Right_Edge Bestfit_Galaxy_Counts\n");
    
    for (j=1; j<SM_RED_NBINS; j++) {
      int64_t bin = i*SM_RED_NBINS+j;
      double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_RED_BPDEX;
      double sm = 0.5*(sm_left+sm_right);
      fprintf(out_smf, "%g %g %g %g %g %g %g %g %g %g %g %g\n", sm, SCALED_TRIVALB(sm_red, conv_factor), TRIVALB(sm_sat_ratio), SCALED_TRIVALB(sm_red_q2, conv_factor), sm_left, sm_right);
      fprintf(out_ssfrs, "%g %g %g %g %g %g %g\n", sm, TRIVALB(sm_ssfr), sm_left, sm_right, bestfit->sm_red[bin]);
      fprintf(out_qf, "%g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, TRIVALB(sm_fq), TRIVALB(sm_fq2), TRIVALB(sm_fq3), sm_left, sm_right, bestfit->sm_red[bin]);
      fprintf(out_qfg, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, TRIVALMB(sm_q_cen_ratio), TRIVALMB(sm_q_sat_ratio), TRIVALMB(sm_q_sat_mw_ratio), TRIVALMB(sm_q_sat_gr_ratio), TRIVALMB(sm_q_sat_cl_ratio), sm_left, sm_right, bestfit->sm_red[bin], bestfit->sm_cen[bin], bestfit->sm_red[bin]-bestfit->sm_cen[bin], bestfit->sm_sat_mw[bin], bestfit->sm_sat_gr[bin], bestfit->sm_sat_cl[bin]);
      fprintf(out_qfg_infall, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, TRIVALMB(sm_fq_sat_mw_infall_ratio), TRIVALMB(sm_fq_sat_gr_infall_ratio), TRIVALMB(sm_fq_sat_cl_infall_ratio), sm_left, sm_right, bestfit->sm_red[bin], bestfit->sm_sat_mw[bin], bestfit->sm_sat_gr[bin], bestfit->sm_sat_cl[bin]);
      fprintf(out_q_infall, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, TRIVALMB(sm_q_cen_sat_diff), TRIVALMB(sm_q_cen_sat_mw_diff), TRIVALMB(sm_q_cen_sat_gr_diff), TRIVALMB(sm_q_cen_sat_cl_diff), sm_left, sm_right, bestfit->sm_red[bin], bestfit->sm_red[bin]-bestfit->sm_cen[bin], bestfit->sm_sat_mw[bin], bestfit->sm_sat_gr[bin], bestfit->sm_sat_cl[bin]);
      fprintf(out_rejuv, "%g %g %g %g %g %g %g\n", sm, TRIVALB(sm_rejuv_ratio), sm_left, sm_right, bestfit->sm_red[bin]);
      fprintf(out_ex_situ, "%g %g %g %g %g %g %g\n", sm, TRIVALB(sm_ex_situ_ratio), sm_left, sm_right, bestfit->sm_red[bin]);

      if (i==bestfit->num_scales-1) {
	snprintf(buffer, 1024, "plots/data/qfs/qf_sm_histories_sm%.2f_a%f.dat", sm, bestfit->scales[i]);
	FILE *out_qfh = check_fopen(buffer, "w");
	fprintf(out_qfh, "#Observed stellar mass range: %g < log10(M*/Msun) < %g\n", sm_left, sm_right);
	fprintf(out_qfh, "#Quenched: SFR / SM < %g / yr\n", QUENCHED_THRESHOLD);
	fprintf(out_qfh, "#Galaxies at z=0 are selected as quenched via observed SSFR.\n");
	fprintf(out_qfh, "#Quenching histories are based on true SSFR.\n");
	fprintf(out_qfh, "#See documentation for explanation of true vs. observed SFRs and SMs.\n");
	fprintf(out_qfh, "#Bestfit_X_Galaxies_Remaining: number of galaxies with most-massive progenitor trees extending to the scale factor in question.\n");
	fprintf(out_qfh, "#Scale_factor F(Q)_given_SF@z=0 Err+ Err- F(Q)_given_Q@z=0 Err+ Err- Bestfit_SF_Galaxies_Remaining Bestfit_Q_Galaxies_Remaining\n");
	for (k=bestfit->num_scales-1; k>=0; k--) {
	  int64_t bin = j*bestfit->num_scales + k;
	  fprintf(out_qfh, "%f %g %g %g %g %g %g %g %g\n", bestfit->scales[k], TRIVALB(sm_mp_sf_qf_ratio), TRIVALB(sm_mp_q_qf_ratio), bestfit->sm_mp_sf_counts[bin], bestfit->sm_mp_q_counts[bin]);
	}
	fclose(out_qfh);
      }
    }
    fclose(out_smf);
    fclose(out_ssfrs);
    fclose(out_qf);
    fclose(out_qfg);
    fclose(out_qfg_infall);
    fclose(out_q_infall);
    fclose(out_rejuv);
    fclose(out_ex_situ);



    snprintf(buffer, 1024, "plots/data/smfs/hires/smf_a%f.dat", bestfit->scales[i]);
    out_smf = check_fopen(buffer, "w");

    //snprintf(buffer, 1024, "plots/data/smfs/hires/smf_a%f.dat", bestfit->scales[i]);
    //out_smf = check_fopen(buffer, "w");

    fprintf(out_smf, "#Observed stellar masses in Msun\n");
    fprintf(out_smf, "#Quenched threshold 1 (Q1): Moustakas/PRIMUS Threshold\n");
    fprintf(out_smf, "#Quenched threshold 2 (Q2): observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_smf, "#Quenched threshold 3 (Q3): Muzzin UVJ quenched threshold\n");
    fprintf(out_smf, "#Number densities in comoving Mpc^-3 dex^-1.\n");
    fprintf(out_smf, "#Log10(SM_Center) Number_Density Err+ Err- Q1_Number_Density Err+ Err- Q2_Number_Density Err+ Err- Q3_Number_Density Err+ Err- SM_Left_Edge SM_Right_Edge\n");
    double reg_conv_factor = pow(BOX_SIZE/h0, -3)*SM_BPDEX;    
    for (j=1; j<SM_NBINS; j++) {
      int64_t bin = i*SM_NBINS+j;
      double sm_left = SM_MIN+j/(double)SM_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_BPDEX;
      double sm = 0.5*(sm_left+sm_right);
      fprintf(out_smf, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, SCALED_TRIVALB(smf, reg_conv_factor), SCALED_TRIVALB(smf_q, reg_conv_factor),
	      SCALED_TRIVALB(smf_q2, reg_conv_factor), SCALED_TRIVALB(smf_q3, reg_conv_factor), sm_left, sm_right);
    }
    fclose(out_smf);

    if (up2 && dn2) {
      snprintf(buffer, 1024, "plots/data/smfs/hires/smf_2sigma_a%f.dat", bestfit->scales[i]);
      out_smf = check_fopen(buffer, "w");
      fprintf(out_smf, "#Observed stellar masses in Msun\n");
      fprintf(out_smf, "#Quenched threshold 1 (Q1): Moustakas/PRIMUS Threshold\n");
      fprintf(out_smf, "#Quenched threshold 2 (Q2): observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
      fprintf(out_smf, "#Quenched threshold 3 (Q3): Muzzin UVJ quenched threshold\n");
      fprintf(out_smf, "#Number densities in comoving Mpc^-3 dex^-1.\n");
      fprintf(out_smf, "#Log10(SM_Center) Number_Density Err+(2sig) Err-(2sig) Q1_Number_Density Err+(2sig) Err-(2sig) Q2_Number_Density Err+(2sig) Err-(2sig) Q3_Number_Density Err+(2sig) Err-(2sig) SM_Left_Edge SM_Right_Edge\n");
      double reg_conv_factor = pow(BOX_SIZE/h0, -3)*SM_BPDEX;    
      for (j=1; j<SM_NBINS; j++) {
	int64_t bin = i*SM_NBINS+j;
	double sm_left = SM_MIN+j/(double)SM_BPDEX;
	double sm_right = sm_left+1.0/(double)SM_BPDEX;
	double sm = 0.5*(sm_left+sm_right);
	fprintf(out_smf, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm, SCALED_TRIVALB2(smf, reg_conv_factor), SCALED_TRIVALB2(smf_q, reg_conv_factor),
		SCALED_TRIVALB2(smf_q2, reg_conv_factor), SCALED_TRIVALB2(smf_q3, reg_conv_factor), sm_left, sm_right);
      }
      fclose(out_smf);
    }
  }
}




void gen_hmfs(void) {
  char buffer[1024];
  check_mkdir("plots/data/hmfs", 0755);
  check_mkdir("plots/data/qfs", 0755);
  check_mkdir("plots/data/rejuvenation", 0755);
  check_mkdir("plots/data/ex_situ", 0755);
  int64_t i, j, k;
  double conv_factor = pow(BOX_SIZE/h0, -3)*HM_M_BPDEX;
  for (i=0; i<bestfit->num_scales; i++) {
    snprintf(buffer, 1024, "plots/data/hmfs/hmf_a%f.dat", bestfit->scales[i]);
    FILE *out_hmf = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_hm_a%f.dat", bestfit->scales[i]);
    FILE *out_qf = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_hm_groupstats_a%f.dat", bestfit->scales[i]);
    FILE *out_qfg = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_hm_groupstats_infall_a%f.dat", bestfit->scales[i]);
    FILE *out_qfg_infall = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/qfs/qf_hm_quenched_infall_a%f.dat", bestfit->scales[i]);
    FILE *out_q_infall = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/rejuvenation/rejuv_hm_a%f.dat", bestfit->scales[i]);
    FILE *out_rejuv = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/ex_situ/ex_situ_hm_a%f.dat", bestfit->scales[i]);
    FILE *out_ex_situ = check_fopen(buffer, "w");
    
    
    fprintf(out_hmf, "#Peak halo masses in Msun\n");
    fprintf(out_hmf, "#Number densities in comoving Mpc^-3 dex^-1.\n");
    fprintf(out_hmf, "#Log10(HM) Number_Density Err+ Err- Satellite_fraction Err+ Err- HM_Left_Edge HM_Right_Edge\n");
    fprintf(out_qf, "#Peak halo masses in Msun\n");
    fprintf(out_qf, "#Bestfit_Halo_Counts is the number of halos in the bin for the bestfit model.\n");
    fprintf(out_qf, "#Log10(HM) FQ(True_SSFR<%g/yr) Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg, "#Peak halo masses in Msun\n");
    fprintf(out_qfg, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_qfg, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_qfg, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_qfg, "#Quenched threshold: true SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg, "#Bestfit_X_Counts is the number of halos in the bin for the bestfit model.\n");
    fprintf(out_qfg, "#Log10(HM) FQ(Centrals) Err+ Err- FQ(Sats) Err+ Err- FQ(Sats_MW) Err+ Err- FQ(Sats_Group) Err+ Err- FQ(Sats_Cluster) Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts  Bestfit_Cen_Halo_Counts Bestfit_Sat_Halo_Counts Bestfit_Sat_MW_Counts Bestfit_Sat_GR_Counts Bestfit_Sat_CL_Counts\n");
    fprintf(out_qfg_infall, "#Peak halo masses in Msun\n");
    fprintf(out_qfg_infall, "#Fractions of *quenched satellites* that were quenched after infall\n");
    fprintf(out_qfg_infall, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_qfg_infall, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_qfg_infall, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_qfg_infall, "#Quenched threshold: true SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_qfg_infall, "#Bestfit_X_Counts is the number of halos in the bin for the bestfit model.\n");	
    fprintf(out_qfg_infall, "#Log10(HM) FQ_infall(Sats_MW) Err+ Err- FQ_infall(Sats_Group) Err+ Err- FQ_infall(Sats_Cluster) Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts  Bestfit_Sat_MW_Counts Bestfit_Sat_GR_Counts Bestfit_Sat_CL_Counts\n");
    fprintf(out_q_infall, "#Peak halo masses in Msun\n");
    fprintf(out_q_infall, "#Fractions of satellites that were quenched due to infall (f_q,sat - f_q,cen)\n");
    fprintf(out_q_infall, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out_q_infall, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out_q_infall, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out_q_infall, "#Quenched threshold: true SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_q_infall, "#Bestfit_X_Counts is the number of galaxies in the bin for the bestfit model.\n");
    fprintf(out_q_infall, "#Log10(HM_Center) FQ_infall(Sats) Err+ Err- FQ_infall(Sats_MW) Err+ Err- FQ_infall(Sats_Group) Err+ Err- FQ_infall(Sats_Cluster) Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts Bestfit_Satellite_Counts Bestfit_Sats_MW_Counts Bestfit_Sats_GR_Counts Bestfit_Sats_Cluster_Counts\n");
    fprintf(out_rejuv, "#Peak halo masses in Msun\n");
    fprintf(out_rejuv, "#Rejuvenation definition: at least 300Myr quenched, followed by at least 300Myr of star formation.\n");
    fprintf(out_rejuv, "#Quenched threshold: %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out_rejuv, "#Bestfit_Halo_Counts is the number of halos in the bin for the bestfit model.\n");
    fprintf(out_rejuv, "#Log10(HM) F_rejuv Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts\n");
    fprintf(out_ex_situ, "#Peak halo masses in Msun\n");
    fprintf(out_ex_situ, "#Average fraction of mass accreted from galaxy mergers as opposed to formed in situ.\n");
    fprintf(out_qf, "#Bestfit_Halo_Counts is the number of halos in the bin for the bestfit model.\n");
    fprintf(out_ex_situ, "#Log10(HM) F_ex_situ Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts\n");

    for (j=0; j<HM_M_NBINS; j++) {
      int64_t bin = i*HM_M_NBINS+j;
      double hm_left = HM_M_MIN+j/(double)HM_M_BPDEX;
      double hm_right = hm_left+1.0/(double)HM_M_BPDEX;
      double hm = 0.5*(hm_left+hm_right);
      fprintf(out_hmf, "%.2f %g %g %g %g %g %g %g %g\n", hm, SCALED_TRIVALB(hm_dist, conv_factor), TRIVALB(hm_sat_ratio), hm_left, hm_right);
      fprintf(out_qf, "%.2f %g %g %g %g %g %g\n", hm, TRIVALB(hm_q_ratio), hm_left, hm_right, bestfit->hm_dist[bin]);
      fprintf(out_qfg, "%.2f %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", hm, TRIVALB(hm_q_cen_ratio), TRIVALB(hm_q_sat_ratio), TRIVALB(hm_q_sat_mw_ratio), TRIVALB(hm_q_sat_gr_ratio), TRIVALB(hm_q_sat_cl_ratio), hm_left, hm_right, bestfit->hm_dist[bin], bestfit->hm_cen_dist[bin], bestfit->hm_dist[bin]-bestfit->hm_cen_dist[bin], bestfit->hm_sat_mw[bin], bestfit->hm_sat_gr[bin], bestfit->hm_sat_cl[bin]);
      fprintf(out_qfg_infall, "%.2f %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", hm, TRIVALB(hm_fq_sat_mw_infall_ratio), TRIVALB(hm_fq_sat_gr_infall_ratio), TRIVALB(hm_fq_sat_cl_infall_ratio), hm_left, hm_right, bestfit->hm_dist[bin], bestfit->hm_sat_mw[bin], bestfit->hm_sat_gr[bin], bestfit->hm_sat_cl[bin]);
      fprintf(out_q_infall, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", hm, TRIVALB(hm_q_cen_sat_diff), TRIVALB(hm_q_cen_sat_mw_diff), TRIVALB(hm_q_cen_sat_gr_diff), TRIVALB(hm_q_cen_sat_cl_diff), hm_left, hm_right, bestfit->hm_dist[bin], bestfit->hm_dist[bin]-bestfit->hm_cen_dist[bin], bestfit->hm_sat_mw[bin], bestfit->hm_sat_gr[bin], bestfit->hm_sat_cl[bin]);
      fprintf(out_rejuv, "%.2f %g %g %g %g %g %g\n", hm, TRIVALB(hm_rejuv_ratio), hm_left, hm_right, bestfit->hm_dist[bin]);
      fprintf(out_ex_situ, "%.2f %g %g %g %g %g %g\n", hm, TRIVALB(hm_ex_situ_ratio), hm_left, hm_right, bestfit->hm_dist[bin]);

      if (i==bestfit->num_scales-1) {
	snprintf(buffer, 1024, "plots/data/qfs/qf_hm_histories_hm%.2f_a%f.dat", hm, bestfit->scales[i]);
	FILE *out_qfh = check_fopen(buffer, "w");
	fprintf(out_qfh, "#Peak halo mass range: %g < log10(Mh/Msun) < %g\n", hm_left, hm_right);
	fprintf(out_qfh, "#Quenched threshold: True SFR / True SM < %g / yr\n", QUENCHED_THRESHOLD);
	fprintf(out_qfh, "#True SFHs in units of Msun/yr.\n");
	fprintf(out_qfh, "#See documentation for explanation of true vs. observed SFRs and SMs.\n");
	fprintf(out_qfh, "#Bestfit_X_Galaxies_Remaining: number of galaxies with most-massive progenitor trees extending to the scale factor in question.\n");
	fprintf(out_qfh, "#Scale_factor F(Q)_given_SF@z=0 Err+ Err- F(Q)_given_Q@z=0 Err+ Err- Bestfit_SF_Halos_Remaining Bestfit_Q_Halos_Remaining\n");
	for (k=bestfit->num_scales-1; k>=0; k--) {
	  int64_t bin = j*bestfit->num_scales + k;
	  fprintf(out_qfh, "%f %g %g %g %g %g %g %g %g\n", bestfit->scales[k], TRIVALB(mp_sf_qf_ratio), TRIVALB(mp_q_qf_ratio), bestfit->mp_sf_counts[bin], bestfit->mp_q_counts[bin]);
	}
	fclose(out_qfh);
      }
    }
    fclose(out_hmf);
    fclose(out_qf);
    fclose(out_qfg);
    fclose(out_qfg_infall);
    fclose(out_q_infall);
    fclose(out_rejuv);
    fclose(out_ex_situ);
  }
}


void gen_uvlfs(void) {
  check_mkdir("plots/data/uvlfs", 0755);
  check_mkdir("plots/data/uvlfs_uncalibrated", 0755);
  check_mkdir("plots/data/uvsm", 0755);
  check_mkdir("plots/data/uvsm_uncalibrated", 0755);
  check_mkdir("plots/data/uv_sfr", 0755);
  check_mkdir("plots/data/uv_sfr_uncalibrated", 0755);
  char buffer[1024];
  int64_t i, j;
  double conv_factor = pow(BOX_SIZE/h0, -3)*UV_BPMAG;
  //printf("#Raw Model Data\n");
  //printf("#Median UV-SM relations\n");
  //printf("#z_min z_max uv_min uv_max Log10(Median_SM)\n");
  for (i=0; i<UVSM_REDSHIFTS; i++) {
    double z_min = i-0.5;
    double z_max = i+0.5;
    if (z_min < 0) z_min = 0;
    char *dir = (z_max < 4) ? "uvsm_uncalibrated" : "uvsm";
    snprintf(buffer, 1024, "plots/data/%s/uvsm_z%.1f-z%.1f.dat", dir, z_min, z_max);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Median UV--stellar mass relations.\n");
    fprintf(out, "#Observed stellar masses in Msun.\n");
    fprintf(out, "#Observed UV luminosities (including dust) are M_1500 [AB].\n");
    if (z_max < 4) fprintf(out, "#Note: Dust corrections are uncalibrated at z<4, so UV luminosities may be incorrect.\n");
    fprintf(out, "#UV_Center Median_SM Err+ Err- UV_Min UV_Max\n");
    for (j=0; j<UVSM_UV_BINS; j++) {
      double uv_min = UVSM_MIN + (double)j/(double)UVSM_BPMAG;
      double uv_max = uv_min + 1.0/(double)UVSM_BPMAG;
      double uv = 0.5*(uv_min+uv_max);
      double med_sm = median_uvsm[i*UVSM_UV_BINS+j];
      if (med_sm > 1) med_sm = log10(med_sm);
      else med_sm = 0;
      int64_t bin = i*UVSM_UV_BINS+j;
      fprintf(out, "%g %g %g %g %g %g\n", uv, TRIVALB(median_uvsm), uv_min, uv_max);
    }
    fclose(out);
  }

  for (i=0; i<bestfit->num_scales; i++) {
    char *dir = (bestfit->scales[i] < 1.0/4.5) ? "uvlfs" : "uvlfs_uncalibrated";
    snprintf(buffer, 1024, "plots/data/%s/uvlf_a%f.dat", dir, bestfit->scales[i]);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Observed UV luminosities (including dust) are M_1500 [AB].\n");
    if (bestfit->scales[i] < 1.0/4.5)
       fprintf(out, "#Note: Dust corrections are uncalibrated at z<4, so UV luminosities may be incorrect.\n");
    fprintf(out, "#Number densities are in comoving Mpc^-3 mag^-1.\n");
    fprintf(out, "#UV_Center Number_Density Err+ Err- UV_Min UV_Max\n");
    for (j=0; j<UV_NBINS; j++) {
      double uv_min = UV_MIN + (double)j/(double)UV_BPMAG;
      double uv_max = uv_min + 1.0/(double)UV_BPMAG;
      double uv = 0.5*(uv_min+uv_max);
      int64_t bin = i*UV_NBINS+j;
      fprintf(out, "%g %g %g %g %g %g\n", uv, SCALED_TRIVALB(uvlf, conv_factor), uv_min, uv_max);
    }
    fclose(out);

    if (up2 && dn2) {
      snprintf(buffer, 1024, "plots/data/%s/uvlf_2sigma_a%f.dat", dir, bestfit->scales[i]);
      out = check_fopen(buffer, "w");
      fprintf(out, "#Observed UV luminosities (including dust) are M_1500 [AB].\n");
      if (bestfit->scales[i] < 1.0/4.5)
	fprintf(out, "#Note: Dust corrections are uncalibrated at z<4, so UV luminosities may be incorrect.\n");
      fprintf(out, "#Number densities are in comoving Mpc^-3 mag^-1.\n");
      fprintf(out, "#UV_Center Number_Density Err+(2sig) Err-(2sig) UV_Min UV_Max\n");
      for (j=0; j<UV_NBINS; j++) {
	double uv_min = UV_MIN + (double)j/(double)UV_BPMAG;
	double uv_max = uv_min + 1.0/(double)UV_BPMAG;
	double uv = 0.5*(uv_min+uv_max);
	int64_t bin = i*UV_NBINS+j;
	fprintf(out, "%g %g %g %g %g %g\n", uv, SCALED_TRIVALB2(uvlf, conv_factor), uv_min, uv_max);
      }
      fclose(out);
    }
  }

  for (i=0; i<bestfit->num_scales; i++) {
    char *dir = (bestfit->scales[i] < 1.0/4.5) ? "uv_sfr" : "uv_sfr_uncalibrated";
    snprintf(buffer, 1024, "plots/data/%s/uv_sfr_a%f.dat", dir, bestfit->scales[i]);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Instrinsic UV luminosities (excluding dust) are M_1500 [AB].\n");
    if (bestfit->scales[i] < 1.0/4.5)
       fprintf(out, "#Note: Dust corrections are uncalibrated at z<4, so UV luminosities may be incorrect.\n");
    fprintf(out, "#True SFRs are in Msun/yr\n");
    fprintf(out, "#UV_Center <True_SFR> Err+ Err- UV_Min UV_Max Bestfit_Counts\n");
    for (j=0; j<UV_SFR_NBINS; j++) {
      double uv_min = UV_SFR_MIN + (double)j/(double)UV_SFR_BPMAG;
      double uv_max = uv_min + 1.0/(double)UV_SFR_BPMAG;
      double uv = 0.5*(uv_min+uv_max);
      int64_t bin = i*UV_SFR_NBINS+j;
      fprintf(out, "%g %g %g %g %g %g %g\n", uv, TRIVALB(uv_sfr_av_sfr), uv_min, uv_max, bestfit->uv_sfr_counts[bin]);
    }
    fclose(out);
  }
  
  snprintf(buffer, 1024, "plots/data/uv_sfr/uv_sfr_m17.dat");
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#Average true SFRs at a UV luminosity of M_1500 = -17 [AB]\n");
  fprintf(out, "#True SFRs are in Msun/yr\n");
  fprintf(out, "#Scale <True_SFR_at_M1500> Err+ Err-\n");
  for (i=0; i<bestfit->num_scales; i++) {
    int64_t bin = i;
    fprintf(out, "%g %g %g %g\n", bestfit->scales[i], TRIVALB(uv_sfr_17));
  }
  fclose(out);
}


void gen_smhm_plots(void) {
  char buffer[1024];
  int64_t j, smhm_j;

  check_mkdir("plots/data/smhm", 0755);
  check_mkdir("plots/data/smhm/median_raw", 0755);
  check_mkdir("plots/data/smhm/median_fits", 0755);
  check_mkdir("plots/data/smhm/averages", 0755);
  check_mkdir("plots/data/smhm/params", 0755);
  PRINT_PARAMS(smhm_med_params);
  PRINT_PARAMS(smhm_med_cen_params);
  PRINT_PARAMS(smhm_med_cen_sf_params);
  PRINT_PARAMS(smhm_med_cen_q_params);
  PRINT_PARAMS(smhm_med_sat_params);
  PRINT_PARAMS(smhm_med_sf_params);
  PRINT_PARAMS(smhm_med_q_params);
  PRINT_PARAMS(smhm_true_med_params);
  PRINT_PARAMS(smhm_true_med_cen_params);
  PRINT_PARAMS(smhm_true_med_cen_sf_params);
  PRINT_PARAMS(smhm_true_med_cen_q_params);
  PRINT_PARAMS(smhm_true_med_sat_params);
  PRINT_PARAMS(smhm_true_med_sf_params);
  PRINT_PARAMS(smhm_true_med_q_params);
  PRINT_PARAMS(smhm_true_icl_med_params);
  PRINT_PARAMS(smhm_true_cen_icl_med_params);
  
  for (smhm_j=0; smhm_j<SMHM_NUM_ZS; smhm_j++) {
    snprintf(buffer, 1024, "plots/data/smhm/median_raw/smhm_a%f.dat", bestfit->smhm_scales[smhm_j]);
    FILE *out = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "plots/data/smhm/median_raw/smhm_scatter_a%f.dat", bestfit->smhm_scales[smhm_j]);
    FILE *out_scatter = check_fopen(buffer, "w");
    numbered_fprintf(out, "#HM Med_All Err+ Err- Med_Cen Err+ Err- Med_Cen_SF Err+ Err- Med_Cen_Q Err+ Err- Med_Sat Err+ Err- Med_SF Err+ Err- Med_Q Err+ Err- True_Med_All Err+ Err- True_Cen Err+ Err- True_Cen_SF Err+ Err- True_Cen_Q Err+ Err- True_Sat Err+ Err- True_SF Err+ Err- True_Q Err+ Err-\n");
    numbered_fprintf(out_scatter, "#HM Med_All Err+ Err- Med_Cen Err+ Err- Med_Cen_SF Err+ Err- Med_Cen_Q Err+ Err- Med_Sat Err+ Err- Med_SF Err+ Err- Med_Q Err+ Err- True_Med_All Err+ Err- True_Cen Err+ Err- True_Cen_SF Err+ Err- True_Cen_Q Err+ Err- True_Sat Err+ Err- True_SF Err+ Err- True_Q Err+ Err-\n");
    int64_t offset = smhm_j*SMHM_FIT_NBINS;
    for (j=0; j<SMHM_FIT_NBINS; j++) {
      int64_t bin = offset+j;
      fprintf(out, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      TRIVALB(smhm_med), TRIVALB(smhm_med_cen),
	      TRIVALB(smhm_med_cen_sf), TRIVALB(smhm_med_cen_q), 
	      TRIVALB(smhm_med_sat), TRIVALB(smhm_med_sf), 
	      TRIVALB(smhm_med_q), TRIVALB(smhm_true_med),
	      TRIVALB(smhm_true_med_cen), TRIVALB(smhm_true_med_cen_sf),
	      TRIVALB(smhm_true_med_cen_q), TRIVALB(smhm_true_med_sat),
	      TRIVALB(smhm_true_med_sf), TRIVALB(smhm_true_med_q)
	      );
      fprintf(out_scatter, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      TRIVALB(smhm_scatter), TRIVALB(smhm_scatter_cen),
	      TRIVALB(smhm_scatter_cen_sf), TRIVALB(smhm_scatter_cen_q), 
	      TRIVALB(smhm_scatter_sat), TRIVALB(smhm_scatter_sf), 
	      TRIVALB(smhm_scatter_q), TRIVALB(smhm_true_scatter),
	      TRIVALB(smhm_true_scatter_cen), TRIVALB(smhm_true_scatter_cen_sf),
	      TRIVALB(smhm_true_scatter_cen_q), TRIVALB(smhm_true_scatter_sat),
	      TRIVALB(smhm_true_scatter_sf), TRIVALB(smhm_true_scatter_q)
	      );
    }
    fclose(out);

    snprintf(buffer, 1024, "plots/data/smhm/median_fits/smhm_a%f.dat", bestfit->smhm_scales[smhm_j]);
    out = check_fopen(buffer, "w");
    numbered_fprintf(out, "#HM Med_All Err+ Err- Med_Cen Err+ Err- Med_Cen_SF Err+ Err- Med_Cen_Q Err+ Err- Med_Sat Err+ Err- Med_SF Err+ Err- Med_Q Err+ Err- True_Med_All Err+ Err- True_Cen Err+ Err- True_Cen_SF Err+ Err- True_Cen_Q Err+ Err- True_Sat Err+ Err- True_SF Err+ Err- True_Q Err+ Err-\n");
    for (j=0; j<SMHM_FIT_NBINS; j++) {
      int64_t bin = offset+j;
      fprintf(out, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      TRIVALB(smhm_med_bestfit), TRIVALB(smhm_med_cen_bestfit),
	      TRIVALB(smhm_med_cen_sf_bestfit), TRIVALB(smhm_med_cen_q_bestfit), 
	      TRIVALB(smhm_med_sat_bestfit), TRIVALB(smhm_med_sf_bestfit), 
	      TRIVALB(smhm_med_q_bestfit), TRIVALB(smhm_true_med_bestfit),
	      TRIVALB(smhm_true_med_cen_bestfit), TRIVALB(smhm_true_med_cen_sf_bestfit),
	      TRIVALB(smhm_true_med_cen_q_bestfit), TRIVALB(smhm_true_med_sat_bestfit),
	      TRIVALB(smhm_true_med_sf_bestfit), TRIVALB(smhm_true_med_q_bestfit)
	      );
    }
    fclose(out);

    snprintf(buffer, 1024, "plots/data/smhm/median_raw/ratios_a%f.dat", bestfit->smhm_scales[smhm_j]);
    out = check_fopen(buffer, "w");
    numbered_fprintf(out, "#HM Cen_All Err+ Err- Sat_All Err+ Err- SF_All Err+ Err- Q_All Err+ Err- Cen_Sat Err+ Err- SF_Q Err+ Err-\n");
    for (j=0; j<SMHM_FIT_NBINS; j++) {
      int64_t bin = offset+j;
      fprintf(out, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      TRIVALB(smhm_med_cen_ratio), TRIVALB(smhm_med_sat_ratio),
	      TRIVALB(smhm_med_sf_ratio), TRIVALB(smhm_med_q_ratio),
	      TRIVALB(smhm_med_cen_sat_ratio), TRIVALB(smhm_med_sf_q_ratio));
    }
    fclose(out);

    snprintf(buffer, 1024, "plots/data/smhm/median_fits/ratios_a%f.dat", bestfit->smhm_scales[smhm_j]);
    out = check_fopen(buffer, "w");
    fprintf(out, "#HM Cen_All Sat_All SF_All Q_All Cen_Sat SF_Q\n");
    for (j=0; j<SMHM_FIT_NBINS; j++) {
      int64_t bin = offset+j;
      fprintf(out, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      TRIVALB(smhm_med_cen_bestfit_ratio), TRIVALB(smhm_med_sat_bestfit_ratio),
	      TRIVALB(smhm_med_sf_bestfit_ratio), TRIVALB(smhm_med_q_bestfit_ratio),
	      TRIVALB(smhm_med_bestfit_cen_sat_ratio), TRIVALB(smhm_med_bestfit_sf_q_ratio));
    }
    fclose(out);

    snprintf(buffer, 1024, "plots/data/smhm/median_fits/smhm_residuals_a%f.dat", bestfit->smhm_scales[smhm_j]);
    out = check_fopen(buffer, "w");
    fprintf(out, "#HM Med_All Med_Cen Med_Sat Med_SF Med_Q Cen-All Sat-All SF-All Q-All Cen-Sat SF-Q True True_Cen\n");
    for (j=0; j<SMHM_FIT_NBINS; j++) {
      int64_t bin = offset+j;
      fprintf(out, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
	      RESIDUAL(smhm_med, smhm_med_bestfit), RESIDUAL(smhm_med_cen, smhm_med_cen_bestfit), 
	      RESIDUAL(smhm_med_sat, smhm_med_sat_bestfit), RESIDUAL(smhm_med_sf, smhm_med_sf_bestfit),
	      RESIDUAL(smhm_med_q, smhm_med_q_bestfit), RESIDUAL(smhm_med_cen_ratio, smhm_med_cen_bestfit_ratio),
	      RESIDUAL(smhm_med_sat_ratio, smhm_med_sat_bestfit_ratio),  RESIDUAL(smhm_med_sf_ratio, smhm_med_sf_bestfit_ratio),
	      RESIDUAL(smhm_med_q_ratio, smhm_med_q_bestfit_ratio), RESIDUAL(smhm_med_cen_sat_ratio, smhm_med_bestfit_cen_sat_ratio),
	      RESIDUAL(smhm_med_sf_q_ratio, smhm_med_bestfit_sf_q_ratio), RESIDUAL(smhm_true_med, smhm_true_med_bestfit),
	      RESIDUAL(smhm_true_med_cen, smhm_true_med_cen_bestfit));
    }
    fclose(out);
  }

  int64_t i;
  for (i=0; i<bestfit->num_scales; i++) {
    snprintf(buffer, 1024, "plots/data/smhm/averages/sm_averages_a%f.dat", bestfit->scales[i]);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Stellar mass averages in bins of peak halo mass.\n");
    fprintf(out, "#Log_SM: Average of Log_10(Observed SM/Msun) in each bin.\n");
    fprintf(out, "#Log_SMHM: Average of Log_10(Observed SM/HM) in each bin.\n");
    fprintf(out, "#Log_True_SM: Average of Log_10(True SM/Msun) in each bin.\n");
    fprintf(out, "#Log_True_SMHM: Average of Log_10(True SM/HM) in each bin.\n");
    fprintf(out, "#Bestfit_Halo_Counts is the number of halos in the bin for the bestfit model.\n");
    numbered_fprintf(out, "#Log10(HM_Center) Log_SM Err+ Err- Log_SMHM Err+ Err- Log_True_SM Err+ Err- Log_True_SMHM Err+ Err- HM_Left_Edge HM_Right_Edge Bestfit_Halo_Counts\n");
    for (j=0; j<HM_M_NBINS; j++) {
      int64_t bin = i*HM_M_NBINS+j;
      double hm_left = HM_M_MIN+j/(double)HM_M_BPDEX;
      double hm_right = hm_left+1.0/(double)HM_M_BPDEX;
      double hm = 0.5*(hm_left+hm_right);
      fprintf(out, "%g %f %f %f %f %f %f %f %f %f %f %f %f %g %g %g\n", hm,
	      TRIVALB(mean_log_sm), TRIVALB(mean_log_smhm_ratio),
	      TRIVALB(mean_log_true_sm), TRIVALB(mean_log_true_smhm_ratio),
	      hm_left, hm_right, bestfit->hm_dist[bin]);
    }
    fclose(out);
    
    snprintf(buffer, 1024, "plots/data/smhm/averages/hm_averages_a%f.dat", bestfit->scales[i]);
    out = check_fopen(buffer, "w");
    fprintf(out, "#Halo mass averages in bins of observed stellar mass.\n");
    fprintf(out, "#For satellites, the satellite peak halo mass is used, rather than the host halo's peak halo mass.\n");
    fprintf(out, "#<Mh>: Linear average of peak halo mass (in Msun) in each bin.\n");
    fprintf(out, "#<Log(Mh)>: Average of Log_10(peak halo mass/Msun) in each bin.\n");
    fprintf(out, "#<WL(Mh)>: <Mh^2/3>^(3/2), i.e., weak-lensing-averaged halo mass; i.e., average of (peak halo mass (in Msun) to the two-thirds power), taken to the 3/2 power.\n");
    fprintf(out, "#Bestfit_X_Counts is the number of galaxies of type X in the bin for the bestfit model.\n");
    numbered_fprintf(out, "#Log10(SM_Center) <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err- <Mh> Err+ Err- <Log(Mh)> Err+ Err- <WL(Mh)> Err+ Err-  HM_Left_Edge HM_Right_Edge Bestfit_Galaxy_Counts Bestfit_Central_Counts Bestfit_Central_SF_Counts Bestfit_Central_Q_Counts Bestfit_Satellite_Counts Bestfit_SF_Counts Bestfit_Q_Counts\n");
    for (j=0; j<SM_RED_NBINS; j++) {
      int64_t bin = i*SM_RED_NBINS+j;
      double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_RED_BPDEX;
      double sm = 0.5*(sm_left+sm_right);
      fprintf(out, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n", sm,
	      TRIVALB(mean_hm), TRIVALB(mean_log_hm), TRIVALB(mean_wl_hm), 
	      TRIVALB(mean_cen_hm), TRIVALB(mean_cen_log_hm), TRIVALB(mean_cen_wl_hm), 
	      TRIVALB(mean_cen_sf_hm), TRIVALB(mean_cen_sf_log_hm), TRIVALB(mean_cen_sf_wl_hm), 
	      TRIVALB(mean_cen_q_hm), TRIVALB(mean_cen_q_log_hm), TRIVALB(mean_cen_q_wl_hm), 
	      TRIVALB(mean_sat_hm), TRIVALB(mean_sat_log_hm), TRIVALB(mean_sat_wl_hm), 
	      TRIVALB(mean_sf_hm), TRIVALB(mean_sf_log_hm), TRIVALB(mean_sf_wl_hm), 
	      TRIVALB(mean_q_hm), TRIVALB(mean_q_log_hm), TRIVALB(mean_q_wl_hm), 
	      sm_left, sm_right, bestfit->sm_red[bin], bestfit->sm_cen[bin],
	      bestfit->sm_cen[bin]-bestfit->sm_q_cen[bin], bestfit->sm_q_cen[bin],
	      bestfit->sm_red[bin]-bestfit->sm_cen[bin], bestfit->sm_red[bin]-bestfit->sm_red_q2[bin],
	      bestfit->sm_red_q2[bin]);
    }
    fclose(out);
  }
}
  


void gen_wl_plots(void) {
  check_mkdir("plots/data/weak_lensing", 0755);
  char buffer[1024];
  int64_t i, j, wl_j;
  double threshes[NUM_WL_THRESHES] = {WL_MIN_SM, WL_MIN_SM2, WL_MIN_SM3};
  assert(NUM_WL_THRESHES==3);

  for (wl_j=0; wl_j<NUM_WL_ZS; wl_j++) {
    for (j=0; j<NUM_WL_THRESHES; j++) {
      snprintf(buffer, 1024, "plots/data/weak_lensing/wl_sm%.2f_a%f.dat", log10(threshes[j]), bestfit->wl_scales[wl_j]);
      FILE *out = check_fopen(buffer, "w");
      fprintf(out, "#Radius[Mpc] Delta_Sigma_All[Msun/pc^2] Err+ Err- Delta_Sigma_SF Err+ Err- Delta_Sigma_Q Err+ Err-\n");
      fprintf(out, "#SM Threshold: log10 M*>%g\n", log10(threshes[j]));
      fprintf(out, "#SSFR Threshold for Quenching: SSFR < %g/yr\n", QUENCHED_THRESHOLD);
      fprintf(out, "#Assumed mass / weak lensing particle: %g Msun\n", WL_PARTICLE_MASS/h0);
      fprintf(out, "#If above is incorrect, multiply all Delta_sigma below by true_particle_mass/%g Msun\n", WL_PARTICLE_MASS/h0);
      snprintf(buffer, 1024, "plots/data/weak_lensing/wl_ratios_sm%.2f_a%f.dat", log10(threshes[j]), bestfit->wl_scales[wl_j]);
      FILE *out_ratios = check_fopen(buffer, "w");
      fprintf(out_ratios, "#Radius[Mpc] Delta_Sigma_Q/Delta_Sigma_SF Err+ Err- Delta_Sigma_SF/Delta_Sigma_All Err+ Err- Delta_Sigma_Q/Delta_Sigma_All Err+ Err-\n");
      fprintf(out_ratios, "#SM Threshold: log10 M*>%g\n", log10(threshes[j]));
      fprintf(out_ratios, "#SSFR Threshold for Quenching: SSFR < %g/yr\n", QUENCHED_THRESHOLD);
      for (i=1; i<NUM_WL_BINS; i++) {
	double r2 = pow(10, WL_MIN_R+i/(double)WL_BPDEX)/h0; //In Mpc
	double r1 = pow(10, WL_MIN_R+(i-1)/(double)WL_BPDEX)/h0; //In Mpc
	double av_r = sqrt(r1*r1+ 0.5*(r2*r2-r1*r1));
	int64_t bin = (wl_j*NUM_WL_THRESHES + j)*NUM_WL_TYPES*NUM_WL_BINS;
	fprintf(out, "%g %g %g %g %g %g %g %g %g %g\n", av_r, TRIVAL(wl_post[bin+i]),
		TRIVAL(wl_post[bin+NUM_WL_BINS+i]), TRIVAL(wl_post[bin+2*NUM_WL_BINS+i]));
	fprintf(out_ratios, "%g %g %g %g %g %g %g %g %g %g\n", av_r, TRIVAL(wl_post_ratios[bin+i]),
		TRIVAL(wl_post_ratios[bin+NUM_WL_BINS+i]), TRIVAL(wl_post_ratios[bin+2*NUM_WL_BINS+i]));
      }
      fclose(out);
      fclose(out_ratios);
    }
  }
}

void print_corr_header(FILE *out, float scale, float sm1, float sm2, float velocity_errors) {
  fprintf(out, "#Scale: %f\n", scale);
  fprintf(out, "#h: %f\n", h0);
  fprintf(out, "#Stellar mass range: 10^%.2f - 10^%.2f Msun\n", log10(sm1), log10(sm2));
  fprintf(out, "#Assumed pi_max: %f Mpc/h\n", POSTPROCESS_CORR_LENGTH_PI);
  fprintf(out, "#Assumed redshift errors: %.1f km/s\n", velocity_errors);
  fprintf(out, "#Quenched Threshold: SSFR < %g / yr.\n", QUENCHED_THRESHOLD);
  fprintf(out, "#All units for R_p and w_p(R_p) are Mpc/h.\n");
}

void gen_corr_plots(void) {
  int64_t i, j, k, l;
  char buffer[1024];
  double threshes[NUM_CORR_THRESHES+1] = {CORR_MIN_SM, POSTPROCESS_CORR_MIN_SM, POSTPROCESS_CORR_MIN_SM2, POSTPROCESS_CORR_MIN_SM3, 1e14};
  assert(NUM_CORR_THRESHES==4);
  check_mkdir("plots/data/corrs", 0755);
  init_dists();
  for (i=0; i<bestfit->num_scales; i++) {
    if (bestfit->scales[i] < POSTPROCESS_CORR_MIN_SCALE) continue;
    for (j=1; j<NUM_CORR_THRESHES; j++) {
      snprintf(buffer, 1024, "plots/data/corrs/corr_sm%.2f-%.2f_a%f.dat", log10(threshes[j]), log10(threshes[j+1]), bestfit->scales[i]);
      FILE *out = check_fopen(buffer, "w");
      snprintf(buffer, 1024, "plots/data/corrs/corr_ratios_sm%.2f-%.2f_a%f.dat", log10(threshes[j]), log10(threshes[j+1]), bestfit->scales[i]);
      FILE *ratios_out = check_fopen(buffer, "w");
      double velocity_errors = REDSHIFT_ERROR_HIGHZ;
      double lowz_errors = REDSHIFT_ERROR;
      if (velocity_errors < 1) velocity_errors *= SPEED_OF_LIGHT / bestfit->scales[i];
      if (lowz_errors < 1) velocity_errors *= SPEED_OF_LIGHT / bestfit->scales[i];
      if (lowz_errors > velocity_errors) velocity_errors = lowz_errors;
      print_corr_header(out, bestfit->scales[i], threshes[j], threshes[j+1], velocity_errors);
      print_corr_header(ratios_out, bestfit->scales[i], threshes[j], threshes[j+1], velocity_errors);
      numbered_fprintf(out, "#Bin_center W_p(all) Err+ Err- W_p(SF) Err+ Err- W_p(Q) Err+ Err- W_p(SFxQ) Err+ Err- Bin_left_edge Bin_right_edge\n");
      numbered_fprintf(ratios_out, "#Bin_center W_p(Q)/W_p(SF) Err+ Err- W_p(SF)/W_p(All) Err+ Err- W_p(Q)/W_p(All) Err+ Err-  W_p(SFxQ)/W_p(All) Err+ Err- Bin_left_edge Bin_right_edge\n");
      for (l=0; l<NUM_BINS; l++) {
	double d1 = sqrt(corr_dists[l]);
	double d2 = sqrt(corr_dists[l+1]);
	double r = sqrt(0.5*(d1*d1+d2*d2));
	fprintf(out, "%g", r);
	fprintf(ratios_out, "%g", r);
	for (k=0; k<NUM_CORR_TYPES; k++) {
	  int64_t bin = i*NUM_CORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS + k*NUM_BINS + l;
	  fprintf(out, " %g %g %g", TRIVALB(cfs_post));
	  fprintf(ratios_out, " %g %g %g", TRIVALB(cf_ratios_post));
	}
	fprintf(out, " %g %g\n", d1, d2);
	fprintf(ratios_out, " %g %g\n", d1, d2);
      }
      fclose(out);
      fclose(ratios_out);
    }
  }
}


void gen_sfh_data(void) {
  int64_t i, j, k;
  char buffer[1024];
  check_mkdir("plots/data/sfhs", 0755);
  for (i=0; i<bestfit->num_scales; i++) {
    struct sf_model c = calc_sf_model(bestfit->model[0], bestfit->scales[i]);
    double conv_factor = pow(10, c.obs_sm_offset);
    for (j=1; j<SM_RED_NBINS; j++) {
      int64_t offset = SM_RED_NBINS*i;
      double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_RED_BPDEX;
      double sm = 0.5*(sm_left+sm_right);
      snprintf(buffer, 1024, "plots/data/sfhs/sfh_sm%.2f_a%f.dat", sm, bestfit->scales[i]);
      FILE *out = check_fopen(buffer, "w");
      fprintf(out, "#Observed stellar mass range: %g < log10(M*/Msun) < %g\n", sm_left, sm_right);
      fprintf(out, "#Quenched threshold: Obs. SFR / Obs. SM < %g / yr\n", QUENCHED_THRESHOLD);
      fprintf(out, "#SFHs in units of observed Msun/yr.\n");
      fprintf(out, "#True SFHs rescaled by average systematic offset of %g dex to be consistent with average observed stellar mass in bin.\n", c.obs_sm_offset);
      fprintf(out, "#See documentation for explanation of true vs. observed SFRs and SMs.\n");
      fprintf(out, "#Bestfit galaxies in bin: %g\n", bestfit->sm_red[offset+j]);
      fprintf(out, "#Bestfit SF galaxies in bin: %g\n", bestfit->sm_red[offset+j]-bestfit->sm_red_q2[offset+j]);
      fprintf(out, "#Bestfit Q galaxies in bin: %g\n", bestfit->sm_red_q2[offset+j]);
      fprintf(out, "#Bestfit central galaxies in bin: %g\n", bestfit->sm_cen[offset+j]);
      fprintf(out, "#Bestfit satellite galaxies in bin: %g\n", bestfit->sm_red[offset+j]-bestfit->sm_cen[offset+j]);
      fprintf(out, "#Avg_scale_factor <SFH_all> Err+ Err- <SFH_SF> Err+ Err- <SFH_Q> Err+ Err- <SFH_Cen> Err+ Err- <SFH_Sat> Err+ Err- Scale_factor_begin Scale_factor_end\n");
      for (k=0; k<bestfit->num_scales; k++) {
	double a1 = (k>0) ? 0.5*(bestfit->scales[k]+bestfit->scales[k-1]) : 0;
	double a2 = (k<bestfit->num_scales-1) ? 0.5*(bestfit->scales[k]+bestfit->scales[k+1]) : bestfit->scales[k];
	int64_t bin = (offset+j)*bestfit->num_scales+k;
	fprintf(out, "%f %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %f %f\n",
		0.5*(a1+a2), SCALED_TRIVALB(sm_sfh_ratio, conv_factor), SCALED_TRIVALB(sm_sfh_sf_ratio, conv_factor),
		SCALED_TRIVALB(sm_sfh_q_ratio, conv_factor), SCALED_TRIVALB(sm_sfh_cen_ratio, conv_factor),
		SCALED_TRIVALB(sm_sfh_sat_ratio, conv_factor), a1, a2);
      }
      fclose(out);
    }

    for (j=0; j<HM_M_NBINS; j++) {
      int64_t offset = HM_M_NBINS*i;
      double hm_left = HM_M_MIN+j/(double)HM_M_BPDEX;
      double hm_right = hm_left+1.0/(double)HM_M_BPDEX;
      double hm = 0.5*(hm_left+hm_right);
      snprintf(buffer, 1024, "plots/data/sfhs/sfh_hm%.2f_a%f.dat", hm, bestfit->scales[i]);
      FILE *out = check_fopen(buffer, "w");
      fprintf(out, "#Peak halo mass range: %g < log10(Mh/Msun) < %g\n", hm_left, hm_right);
      fprintf(out, "#Quenched threshold: True SFR / True SM < %g / yr\n", QUENCHED_THRESHOLD);
      fprintf(out, "#True SFHs in units of Msun/yr.\n");
      fprintf(out, "#See documentation for explanation of true vs. observed SFRs and SMs.\n");
      fprintf(out, "#Bestfit halos in bin: %g\n", bestfit->hm_dist[offset+j]);
      fprintf(out, "#Bestfit SF halos in bin: %g\n", bestfit->hm_dist[offset+j]-bestfit->hm_q[offset+j]);
      fprintf(out, "#Bestfit Q halos in bin: %g\n", bestfit->hm_q[offset+j]);
      fprintf(out, "#Bestfit central halos in bin: %g\n", bestfit->hm_cen_dist[offset+j]);
      fprintf(out, "#Bestfit satellite halos in bin: %g\n", bestfit->hm_dist[offset+j]-bestfit->hm_cen_dist[offset+j]);
      fprintf(out, "#Avg_scale_factor <SFH_all> Err+ Err- <SFH_SF> Err+ Err- <SFH_Q> Err+ Err- <SFH_Cen> Err+ Err- <SFH_Sat> Err+ Err- Scale_factor_begin Scale_factor_end\n");
      for (k=0; k<bestfit->num_scales; k++) {
	double a1 = (k>0) ? 0.5*(bestfit->scales[k]+bestfit->scales[k-1]) : 0;
	double a2 = (k<bestfit->num_scales-1) ? 0.5*(bestfit->scales[k]+bestfit->scales[k+1]) : bestfit->scales[k];
	int64_t bin = (offset+j)*bestfit->num_scales+k;
	fprintf(out, "%f %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %f %f\n",
		0.5*(a1+a2), TRIVALB(hm_sfh_ratio), TRIVALB(hm_sfh_sf_ratio),
		TRIVALB(hm_sfh_q_ratio), TRIVALB(hm_sfh_cen_ratio),
		TRIVALB(hm_sfh_sat_ratio), a1, a2);
      }
      fclose(out);
    }
  }
}


void print_infall_header(FILE *out, int64_t i, char *name, char *units, char *type) {
    fprintf(out, "#Observed stellar masses in Msun\n");
    fprintf(out, "%s", units);
    fprintf(out, "#MW Mass Range: 10^11.75 - 10^12.25 Msun.\n");
    fprintf(out, "#Group Mass Range: 10^12.75 - 10^13.25 Msun.\n");
    fprintf(out, "#Cluster Mass Range: 10^13.75 - 10^14.25 Msun.\n");
    fprintf(out, "#Err+ and Err- are errors over all MCMC points.\n");
    fprintf(out, "#+1 and -1 sigma are the 84th and 16th percentile ranges for galaxies in a single model.\n");
    fprintf(out, "#All satellites selected at a=%f.\n", bestfit->scales[i]);
    fprintf(out, "#Quenched threshold: observed SSFR < %g/yr\n", QUENCHED_THRESHOLD);
    fprintf(out, "#Bestfit_X_Counts is the number of %sgalaxies in the bin for the bestfit model.\n", type);
    fprintf(out, "#SM Median_%s(MW) Err+ Err- +1_sigma_%s(MW) Err+ Err- -1_sigma_%s(MW) Err+ Err- Median_%s(GR) Err+ Err- +1_sigma_%s(GR) Err+ Err- -1_sigma_%s(GR) Err+ Err- Median_%s(CL) Err+ Err- +1_sigma_%s(CL) Err+ Err- -1_sigma_%s(CL) Err+ Err- SM_Left SM_Right Bestfit_Galaxy_Counts Bestfit_Sats_MW_Counts Bestfit_Sats_GR_Counts Bestfit_Sats_Cluster_Counts\n", name, name, name, name, name, name, name, name, name);
}


int64_t find_smhm_j(int64_t i) {
  int64_t j;
  for (j=0; j<SMHM_NUM_ZS; j++)
    if (fabs(bestfit->scales[i]-bestfit->smhm_scales[j]) < 1e-4) return j;
  return -1;
}

float count_quenched_after_infall(int64_t i, int64_t j, float *array) {
  int64_t offset = TS_NBINS*(i*SM_RED_NBINS+j);
  int64_t counts=0;
  for (int64_t k=0; k<TS_NBINS; k++) counts += array[offset+k];
  return (float)counts;
}

void gen_infall_stats(void) {
  int64_t i, j;
  check_mkdir("plots/data/infall_stats", 0755);
  char buffer[1024];
  
  for (i=0; i<bestfit->num_scales; i++) {
    snprintf(buffer, 1024, "plots/data/infall_stats/quenching_delay_times_a%f.dat", bestfit->scales[i]);
    FILE *out = check_fopen(buffer, "w");
    print_infall_header(out, i, "Quenching_Delay_Time", "#Quenching delay times in Gyr.\n", "quenched(all)/quenched after infall(sats) ");
    snprintf(buffer, 1024, "plots/data/infall_stats/infall_delay_times_a%f.dat", bestfit->scales[i]);
    FILE *out_inf = check_fopen(buffer, "w");
    print_infall_header(out_inf, i, "Infall_Delay_Time", "#Infall delay times in Gyr.\n", "");
    
    int64_t smhm_j = find_smhm_j(i);
    FILE *out_ssfrs = NULL;
    if (smhm_j > -1) {
      snprintf(buffer, 1024, "plots/data/infall_stats/infall_ssfrs_a%f.dat", bestfit->scales[i]);
      out_ssfrs = check_fopen(buffer, "w");
      print_infall_header(out_ssfrs, i, "Infall_SSFRs", "#Average Infall SSFRs in yr^-1.\n", "");
    }

    for (j=1; j<SM_RED_NBINS; j++) {
      double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_RED_BPDEX;
      double sm = 0.5*(sm_left+sm_right);
      int64_t bin = 3*(i*SM_RED_NBINS+j);
      int64_t cbin = (i*SM_RED_NBINS+j);
      fprintf(out, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
	      sm, TRIVALM(sm_td_sat_mw_infall_med[bin]), TRIVALM(sm_td_sat_mw_infall_med[bin+1]), TRIVALM(sm_td_sat_mw_infall_med[bin+2]),
	      TRIVALM(sm_td_sat_gr_infall_med[bin]), TRIVALM(sm_td_sat_gr_infall_med[bin+1]), TRIVALM(sm_td_sat_gr_infall_med[bin+2]),
	      TRIVALM(sm_td_sat_cl_infall_med[bin]), TRIVALM(sm_td_sat_cl_infall_med[bin+1]), TRIVALM(sm_td_sat_cl_infall_med[bin+2]),
	      sm_left, sm_right, bestfit->sm_red_q2[cbin], count_quenched_after_infall(i, j, bestfit->sm_td_sat_mw_infall),
	      count_quenched_after_infall(i, j, bestfit->sm_td_sat_gr_infall), count_quenched_after_infall(i, j, bestfit->sm_td_sat_cl_infall));

      fprintf(out_inf, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
	      sm, TRIVALM(sm_ti_sat_mw_infall_med[bin]), TRIVALM(sm_ti_sat_mw_infall_med[bin+1]), TRIVALM(sm_ti_sat_mw_infall_med[bin+2]),
	      TRIVALM(sm_ti_sat_gr_infall_med[bin]), TRIVALM(sm_ti_sat_gr_infall_med[bin+1]), TRIVALM(sm_ti_sat_gr_infall_med[bin+2]),
	      TRIVALM(sm_ti_sat_cl_infall_med[bin]), TRIVALM(sm_ti_sat_cl_infall_med[bin+1]), TRIVALM(sm_ti_sat_cl_infall_med[bin+2]),
	      sm_left, sm_right, bestfit->sm_red[cbin], bestfit->sm_sat_mw[cbin], bestfit->sm_sat_gr[cbin], bestfit->sm_sat_cl[cbin]);

      if (out_ssfrs) {
	int64_t sbin = 3*(smhm_j*SM_RED_NBINS+j);
	fprintf(out_ssfrs, "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
		sm, TRIVALM(sm_ssfr_sat_mw_infall_med[sbin]), TRIVALM(sm_ssfr_sat_mw_infall_med[sbin+1]), TRIVALM(sm_ssfr_sat_mw_infall_med[sbin+2]),
		TRIVALM(sm_ssfr_sat_gr_infall_med[sbin]), TRIVALM(sm_ssfr_sat_gr_infall_med[sbin+1]), TRIVALM(sm_ssfr_sat_gr_infall_med[sbin+2]),
		TRIVALM(sm_ssfr_sat_cl_infall_med[sbin]), TRIVALM(sm_ssfr_sat_cl_infall_med[sbin+1]), TRIVALM(sm_ssfr_sat_cl_infall_med[sbin+2]),
		sm_left, sm_right, bestfit->sm_red[cbin], bestfit->sm_sat_mw[cbin], bestfit->sm_sat_gr[cbin], bestfit->sm_sat_cl[cbin]);
      }
    }
    fclose(out);
    fclose(out_inf);
    if (out_ssfrs) fclose(out_ssfrs);
  }
}
