#!/usr/bin/perl -w
use XmGrace;

our @times = ("0.9903", "0.7643", "0.6643", "0.5843", "0.5243", "0.5043");
our $box_mpc = 80;
our $extra_time_cutoff = 0.45;
our @extra_times = ("0.4043", "0.3443", "0.2843", "0.2443", "0.2243", "0.2043");
our @smass_scatters = ("0.0", "1.0");
our @systematics = (0, 1);
our %systematics_names = (0 => "n", 1 => "");
our @mcmc_types = ("", "_cfit", "_cfit2");

#our $sun_txt = '\sO\m{n}\z{0.5}\v{0.365}\h{-0.945}\x\#{b7}\M{n}\N\f{}';
our $sun_txt = '\v{-0.3}\z{1.0}\f{Courier}o\z{0.5}\v{0.37}\h{-0.90}.\h{-0.3} \v{}\f{}\z{}';
our $large_sun_txt = '\z{1.5}\f{Courier}o\z{0.5}\v{0.37}\h{-0.90}.\h{-0.3} \v{}\f{}\z{}';
our $large_earth_txt = '\z{1.5}\f{Courier}o\z{0.85}\h{-0.65}\v{-0.042}+\f{}\v{}\z{}\h{}';
our $large_jupiter_txt = '2\v{-0.18}\h{-0.15}-\h{-0.3}\v{-0.05}|';
our $msun_txt = '[M'.$sun_txt.']';
our $nd_txt = '[Mpc\S-3\N dex\S-1\N]';
our $ndm_txt = '[Mpc\S-3\N mag\S-1\N]';
our $sfr_txt = '[M'.$sun_txt.' yr\S-1\N]';
our $ssfr_txt = '[yr\S-1\N]';
our $sfr_nd_txt = '[M'.$sun_txt.' yr\S-1\N Mpc\S-3\N]';
our $mw_sats = 'Milky-Way Satellites';
our $gr_sats = 'Group Satellites';
our $cl_sats = 'Cluster Satellites';

our $uv_label = 'M\s1500\N [AB mag]';
our $time_label = 'Time [Gyr]';
our $lbtime_label = 'Lookback Time [Gyr]';
our $sfh_label = 'SFR History '.$sfr_txt;
our $sm_label = 'Stellar Mass '.$msun_txt;
our $dsigma_label = '\xDS\f{}(R\sp\N) [M'.$sun_txt.' pc\S-2\N]';
our $smscatter_label = '\xs\f{}\sM\s*\N [dex]';
our $ohm_label = 'Halo Mass '.$msun_txt;
our $hm_label = 'Peak Halo Mass '.$msun_txt;
our $smhm_ratio_label = 'Stellar - Peak Halo Mass Ratio (M\s*\N / M\sh\N)';
our $vmp_label = 'V\sMpeak\N [km s\S-1\N]',
our $sfr_label = 'SFR '.$sfr_txt;
our $ssfr_label = 'Specific SFR '.$ssfr_txt;
our $csfr_label = 'Cosmic SFR '.$sfr_nd_txt;
our $nd_label = 'Number Density '.$nd_txt;
our $ndm_label = 'Number Density '.$ndm_txt;
our $qf_label = "Quenched Fraction";
our $wp_label = 'w\sp\N(R\sp\N) [Mpc]';
our $rp_label = 'R\sp\N [Mpc]';
our $shear_label = '\xDS\f{}(R\sp\N) [M'.$sun_txt.' pc\S-2\N]',
our @x_ztime = ( xmin => 1,
		 xmax => 11,
		 xticklabels => { qw/1 0 1.5 0.5 2 1 3 2 4 3 5 4 6 5 7 6 8 7 9 8 10 9 11 10/ },
		 altxaxis => "on",
		 altxticklabels => { XmGrace::timespec_gyr(1,2,4,7,10) },
		 altxlabel => $time_label,
		 altxtickdir => "out",
		 xminorticks => 2,
		 xscale => 'Logarithmic',
		 xlabel => 'z');

our @x_zlabel = ( xmin => 1,
		 xmax => 11,
		 xticklabels => { qw/1 0 1.5 0.5 2 1 3 2 4 3 5 4 6 5 7 6 8 7 9 8 10 9 11 10/ },
		 xminorticks => 2,
		 xscale => 'Logarithmic',
		 xlabel => 'z');

our @x_lbtime = (xlabel => $lbtime_label,
		 xscale => 'Normal',
#           text => [{pos => "0.8, 0.8", color => 0, text => "2.0"}],
		 altxaxis => "on",
		 altxticklabels => { XmGrace::gen_z_timespec_lb(0,0.2, 0.5, 1,2,4,8) },
		 altxlabel => "z",
		 altxtickdir => "out",
		 xmax => 14,
		 xmin => 0.0,
		 xticklabels => { qw/0 0 1 1 2 2 4 4 6 6 8 8 10 10 12 12 13.86 13.8/ },
		 #xmajortick => 2,
		 xminorticks => 1,
    );

our @x_time = (xlabel => "Time Since Big Bang [Gyr]",
	       xscale => 'Normal',
	       #           text => [{pos => "0.8, 0.8", color => 0, text => "2.0"}],
	       altxaxis => "on",
	       altxticklabels => { XmGrace::gen_z_timespec(0,0.2, 0.5, 1,2,4,8) },
	       altxlabel => "z",
	       altxtickdir => "out",
	       xmax => 14,
	       xmin => 0.5,
	       xticklabels => { qw/1 1 2 2 4 4 6 6 8 8 10 10 12 12 13.86 13.8/ },
	       #xmajortick => 2,
	       xminorticks => 1,
    );

our $sigma_hat = '\xs\f{}\dR\v{0.3}^\v{-0.3}\dL\h{0.5}';
our %scatter_names = ("0.0" => "$sigma_hat = 0", "1.0" => "$sigma_hat = 1");
our %simple_plots = ("0.9903" => "xydydy", "0.6643" => "xy", "0.5043" => "xydydy");
our $matching_samples = 2**16;


our %models = ( mgauss_cen => "CW08 SFR", power_cen => "Evolving Power Law SFR", behroozi_cen => "Modified CW08 SFR");
our %model_numbers = (mgauss_cen => 0, behroozi_cen => 1, power_cen => 2,
		      power => 2, power_mr => 3, power_star => 4,
		      power_time => 5, power_mass => 6);

#( gauss_cen => "Log-Normal SFR", mgauss_cen => "CW08 SFR", power_cen => "Evolving Power Law SFR", power => "Evolving Power Law SFR w/Shock Model", power_time => 'Power Law SFR w/Tau Sat Model', power_mass => 'Power Law SFR w/Mass Sat Model', power_mr => 'Power Law SFR w/Mass Ratio Sat Model', power_star => 'Power Law SFR w/Starburst Model');
our %types = ( cen => "Galaxies", sat => "Satellite Galaxies");
our (%model_dir, %model_data, %model_eps, %model_png, %model_agr);

#Mass cutoffs for box
our $low_mass_cutoff = 10;
our $high_mass_cutoff = 14;

foreach (keys %models) {
    $model_dir{$_} = "models/$_";
    $model_data{$_} = "models/$_/auto_data";
    $model_eps{$_} = "models/$_/EPS";
    $model_png{$_} = "models/$_/PNG";
    $model_agr{$_} = "models/$_/AGR";
}

sub flip_uv {
    my ($x, @rest) = split(" ", $_[0]);
    $x = -$x;
    return "$x @rest";
}

sub uv_labels {
    my ($min, $max) = @_;
    return { map { $_ => -$_ } ($min..$max) };
}

1;
