#!/usr/bin/perl -w
package Universe::Distance;
use Math::Trig;

BEGIN {
    $MAX_Z=100.0;
    $Z_BINS=1000.0;
    $TOTAL_BINS=($MAX_Z*$Z_BINS);
    $Omega_M = 0.307;
    $Omega_L = 1-$Omega_M;
    $h = 0.68;
    $Dh = 2997.92458 / $h; #In Mpc  (Speed of light / H0)
    our @_Dc;
}

init_cosmology($Omega_M, $Omega_L, $h);

sub _E {
    my $z = shift;
    my $z1 = 1.0+$z;
    return sqrt($Omega_M * ($z1*$z1*$z1) + $Omega_L);
}

sub init_cosmology {
    my ($omega_m, $omega_l, $h0) = @_;
    my $Dc_int = 0;
    $Omega_M = $omega_m;
    $Omega_L = $omega_l;
    $h = $h0;
    $Dh = 2997.92458 / $h; #In Mpc  (Speed of light / H0)
    for (my $i=0; $i<$TOTAL_BINS; $i++) {
	my $z = ($i+0.5) / $Z_BINS;
	$_Dc[$i] = $Dc_int * $Dh;
	$Dc_int += 1.0 / (_E($z)*$Z_BINS);
    }
}

sub redshift {
    my $a = shift;
    return (1.0/$a-1.0);
}

sub scale_factor {
    my $z = shift;
    return (1.0/(1.0+$z));
}

sub comoving_distance {
    my $z = shift;
    my $f = $z*$Z_BINS;
    my $bin = int($f);
    if ($z<0) { return 0; }
    if ($bin>($TOTAL_BINS-2)) { return ($_Dc[$TOTAL_BINS-1]); }
    $f -= $bin;
    return ($_Dc[$bin]*(1.0-$f) + $_Dc[$bin+1]*$f);
}

sub comoving_distance_h {
  return (comoving_distance($_[0])*$h);
}

sub transverse_distance {
  return (comoving_distance($_[0]));
}

sub angular_diameter_distance {
  return (transverse_distance($_[0]) / (1.0 + $_[0]));
}

sub luminosity_distance {
  return ((1.0+$_[0])*transverse_distance($_[0]));
}

sub comoving_volume_element {
    my $z = shift;
    my $z1da = (1.0+$z)*angular_diameter_distance($z);
    return ($Dh*($z1da*$z1da)/_E($z));
}

sub comoving_volume {
    my $z = shift;
    my $r = transverse_distance($z);
    return (4.0*pi*$r*$r*$r/3.0);
}

sub comoving_volume_to_redshift {
    my $Vc = shift;
    my $r = ($Vc*(3.0/(4.0*pi)))**(1/3);
    if ($r<=0) { return 0; }
    my $z = 1;
    my $dz = 0.1;
    my $rt;
    while ($dz > 1e-7) {
	$rt = transverse_distance($z);
	$dz = ($r - $rt) * $dz/(transverse_distance($z+$dz) - 
				transverse_distance($z));
	if ($z+$dz < 0) { $z /= 3.0; }
	else { $z+=$dz; }
	$dz = abs($dz);
	if ($dz>0.1) { $dz = 0.1; }
    }
    return $z;
}

1;

