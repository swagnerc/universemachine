#ifndef _SAMPLER_H_
#define _SAMPLER_H_

#define GW_SAMPLER 0
#define DJ_SAMPLER 1

struct Walker {
  int64_t dim, done;
  double inv_temp, *params;
  int64_t accepted, rejected;
  double chi2, accepted_chi2, z;
  int64_t step_mode;
  double dlnl_step;
  //For multimodal operation
  double sphere_size, *avg_pos;
  double theta;
  double *step;
  int64_t nsteps, new_dir;
};

struct EnsembleSampler {
  int64_t num_walkers, num_half, dim, cur_walker, walkers_done;
  int64_t accepted, rejected;
  int64_t num_random_steps;
  double a; //The proposal scale parameter
  double inv_temp; //The sampler inverse temperature
  struct Walker *w, *w1, *w2;
  int64_t mode, avg_steps;
  int64_t static_dim;

  int64_t freeze_cov_matrix;
  double *avg, *step, *best_params;
  double *cov_matrix, *orth_matrix, *eigenvalues;
};

struct EnsembleSampler *new_sampler(int64_t num_walkers, int64_t dim, double a);
void free_sampler(struct EnsembleSampler *e);
void init_walker_position(struct Walker *w, double *params);
struct Walker *get_next_walker(struct EnsembleSampler *e);
void update_walker(struct EnsembleSampler *e, struct Walker *w, double chi2);
int64_t ensemble_ready(struct EnsembleSampler *e);
void get_accepted_step(struct Walker *w, double *params, double *chi2);
void write_accepted_steps_to_file(struct EnsembleSampler *e, FILE *out, int64_t binary);
void update_ensemble(struct EnsembleSampler *e);
double acceptance_fraction(struct EnsembleSampler *e);
void sampler_reset_averages(struct EnsembleSampler *e);
void sampler_init_averages(struct EnsembleSampler *e);
void sampler_random_step(struct EnsembleSampler *e, struct Walker *w);
void sampler_stats_to_step(struct EnsembleSampler *e);

double *autocorrelation(double *params, int64_t nwalkers, int64_t total_steps);
void print_autocorrelation(double *params, int64_t nwalkers, int64_t total_steps, FILE *out);

#endif /* _SAMPLER_H_ */
