#ifndef _STATS_H_
#define _STATS_H_

#define STATS_DEFAULT_REBIN_THRESH 1000000

#ifndef STATS_SAVEMEM
#define STATS_BIN_TYPE int64_t
#define STATS_VAL_TYPE double
#else
#define STATS_BIN_TYPE int32_t
#define STATS_VAL_TYPE float
#endif /* STATS_SAVEMEM */

struct fullval {
  STATS_BIN_TYPE bin;
  STATS_VAL_TYPE val;
};

struct binstats {
  double bin_start, bin_end, bpunit;
  double *keyavg, *avg, *m2, *sd;
  double *med, *stdup, *stddn;
  double *avgsd;
  struct fullval *fullvals;
  STATS_VAL_TYPE **bvals;
  int64_t *alloced_bvals;
  int64_t *counts;
  int64_t num_fullvals, alloced_fullvals;
  int64_t fullstats, num_bins, logarithmic;
  int64_t rebin_thresh;
  char **names;
};

struct binstats *init_binstats(double bin_start, double bin_end, double bpunit,
			       int64_t fullstats, int64_t logarithmic);
void clear_binstats(struct binstats *bs);
void free_binstats(struct binstats *bs);
int64_t _update_binstats(struct binstats *bs, int64_t bin, double keyval, double val);
void add_to_binstats(struct binstats *bs, double key, double val);
void binstats_prealloc(struct binstats *bs, int64_t vals, int64_t prewrite);
void calc_median(struct binstats *bs);
void print_avgs(struct binstats *bs, FILE *output);
void print_medians(struct binstats *bs, FILE *output);
void calc_jackknife_errs(struct binstats *bs);
void set_bin_names(struct binstats *bs, char **names);
void print_keyname(struct binstats *bs, int64_t bin, FILE *output);
void print_5stats(struct binstats *bs, int64_t bin, FILE *output);

#ifndef STATS_SAVEMEM
struct correlval { double v[2]; };
#else
struct correlval { float v[2]; };
#endif /*STATS_SAVEMEM*/

struct correl_stats {
  int64_t counts, alloced, resamples;
  double xavg, yavg, m2x, m2y, sdx, sdy, xy;
  double rho, rho_up, rho_dn, rho_se;
  double rank_rho, rank_rho_up, rank_rho_dn, rank_rho_se;
  struct correlval *cvals;
};

void clear_correl(struct correl_stats *cs);
void free_correl(struct correl_stats *cs);
struct correl_stats *init_correl(int64_t bootstrap_samples);
void add_to_correl(struct correl_stats *cs, double x, double y);
void calc_correl(struct correl_stats *cs);
void calc_rank_correl(struct correl_stats *cs);
void print_correl(struct correl_stats *cs, FILE *output);
void print_rank_correl(struct correl_stats *cs, FILE *output);

struct binned_correl {
  double bin_start, bin_end, bpunit;
  double *keyavg;
  struct correl_stats *cstats;
  int64_t num_bins, logarithmic, resamples;
  char **names;
};

struct binned_correl *init_binned_correl(double bin_start, double bin_end, double bpunit,
					 int64_t bootstrap_samples, int64_t logarithmic);
void clear_binned_correl(struct binned_correl *bc);
void free_binned_correl(struct binned_correl *bc);
void add_to_binned_correl(struct binned_correl *bc, double key, double x, double y);
void set_bin_names_correl(struct binned_correl *bc, char **names);
void print_binned_correl(struct binned_correl *bc, FILE *output, int64_t print_ranked);

#endif /* _STATS_H_ */
