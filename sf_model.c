#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sf_model.h"

void assert_model_default(struct sf_model_allz *a) {
  OBS_SM_SIG(*a) = 0.07;
  RDECAY(*a) = 1;
}

void assert_model_no_systematics(struct sf_model_allz *a) {
  assert_model_default(a);
  MU(*a) = 0;
  MU_A(*a) = 0;
  KAPPA_A(*a) = 0;
}


int64_t test_invalid(struct sf_model_allz m) {
  struct sf_model c = calc_sf_model(m, 1.0/(10.0+1.0));
  struct sf_model d = calc_sf_model(m, 1.0/(2.0+1.0));
  if (fabs(KAPPA_A(m))>KAPPA_PRIOR*2.5) return 1;
  if (ALPHA(m) >= -1 || c.alpha >= -1 || d.alpha >= -1) return 2;
  if (ALPHA(m) <= -15 || c.alpha <= -15 || d.alpha <= -15) return 3;
  if (BETA(m) < ALPHA(m) || c.beta < c.alpha || d.beta < d.alpha) return 4;
  if (BETA(m) > 15 || c.beta > 15 || d.beta > 15) return 5;
  if (DELTA(m) < 0 || c.delta < 0 || d.delta < 0) return 6;
  if (fabs(DELTA(m)) > 10 || fabs(c.delta) > 10 || fabs(d.delta) > 10) return 7;
  if ((GAMMA(m) > 2) || (fabs(c.gamma) > 50)) return 8;
  if (fabs(d.gamma) > 50 || GAMMA(m) < -10) return 9;
  if (EFF_0(m) > 1 || c.epsilon > 1e5) return 10;
  if ((R_CENTER(m) < 0 || R_CENTER(m) > 4) || (fabs(R_WIDTH(m)) > 10)) return 11;
  if ((fabs(R_MIN(m)) > 1) || (fabs(R_CENTER_A(m)) > 8)) return 12;
  if ((INTR_SCATTER_SF_UNCORR(m) < 0) || (INTR_SCATTER_SF_UNCORR(m) > 1)) return 13;
  if (c.fq_min > 1) return 14;
  if (Q_SIG_LVMP(m) <= 0 || Q_SIG_LVMP(m) > 4) return 15;
  if (c.q_sig_lvmp <= 0 || c.q_sig_lvmp > 4 || d.q_sig_lvmp > 4) return 16;
  if (ICL_DIST(m) < 0.0001 || ICL_DIST(m) > 2.0) return 17;
  if (OBS_SM_SIG(m) > 0.4 || OBS_SM_SIG(m) < 0.001) return 18;
  if (c.obs_sm_sig > 1.5 || c.obs_sm_sig < 0.001) return 19;
  if (fabs(c.obs_sm_offset) > 1 || fabs(MU(m)) > 1) return 20;
  if (fabs(c.obs_sfr_offset) > 1) return 21;
  if (DUST_M(m) > 12 || DUST_M(m) < 6) return 22;
  if (DUST_WIDTH(m) < 0.001 || DUST_WIDTH(m) > 10) return 23;
  if (fabs(DUST_M_Z(m)) > 1.0) return 24;
  if (ORPHAN_THRESH_V1(m) < 0.2 || ORPHAN_THRESH_V1(m) > 1) return 25;
  if (ORPHAN_THRESH_V2(m) < 0.2 || ORPHAN_THRESH_V2(m) > 1) return 26;
  if (RDECAY(m) < 0.001) return 27;
  return 0;
}


double scatter_correction(double sigma) {
  return exp(0.5*sigma*sigma*M_LN10*M_LN10);
}

struct sf_model calc_sf_model(struct sf_model_allz f, double a) {
  struct sf_model c = {0};
  double z = 1.0/a - 1.0;
  double z_ceiling = z;
  if (z_ceiling > 20) z_ceiling = 20;

  double flow = 1.0-a;
  double fmid = log(1.0+z) - flow;
  double fhigh = z - flow;

  double flow20 = 1.0 - (1.0/(z_ceiling+1.0));
  double fmid20 = log(1.0+z_ceiling) - flow20;
  double fhigh20 = z_ceiling - flow20;

  double fsfr = exp(-0.5*((z-2.0)*(z-2.0)));

  //SFR(Vmp)
  c.epsilon = exp(M_LN10*(EFF_0(f) + EFF_0_A(f)*flow20 + EFF_0_A2(f)*fhigh20 + EFF_0_A3(f)*fmid20));
  c.v_1   = V_1(f)   + flow*V_1_A(f)   + fhigh*V_1_A2(f) + V_1_A3(f)*fmid;
  c.alpha = ALPHA(f) + flow20*ALPHA_A(f) + fhigh20*ALPHA_A2(f) + ALPHA_A3(f)*fmid20;

  c.beta  = BETA(f)  + flow20*BETA_A(f)  + fhigh20*BETA_A2(f);
  c.delta = DELTA(f);
  c.gamma = exp(M_LN10*(GAMMA(f) + flow20*GAMMA_A(f) + fhigh20*GAMMA_A2(f)));

  //Dispersion in SFR
  c.ssfr_q = Q_SSFR;
  c.sig_sf = INTR_SCATTER_SF(f) + flow*INTR_SCATTER_SF_A(f);

  if (c.sig_sf > OBS_SCATTER_SFR_SF) c.sig_sf = OBS_SCATTER_SFR_SF;
  if (c.sig_sf < 0) c.sig_sf = 0;

  float sig_rest = (c.sig_sf >= OBS_SCATTER_SFR_SF) ? 0 : sqrt(OBS_SCATTER_SFR_SF*OBS_SCATTER_SFR_SF - c.sig_sf*c.sig_sf);
  c.sig_sf_uncorr = INTR_SCATTER_SF_UNCORR(f)*sig_rest;
  float scatter_tot = sqrt(c.sig_sf*c.sig_sf + c.sig_sf_uncorr*c.sig_sf_uncorr);

  //Rank conversions
  c.r_cen = R_CENTER(f) + R_CENTER_A(f)*flow;
  c.r_width = R_WIDTH(f);
  c.r_min = R_MIN(f);

  c.orphan_thresh_min = ORPHAN_THRESH_V1(f);
  c.orphan_thresh_change = ORPHAN_THRESH_V2(f) - ORPHAN_THRESH_V1(f);

  float rest = 1.0 - fabs(c.r_min);
  c.ra = (rest > 0) ? sqrt(rest) : 0;

  //Quenched fractions
  c.fq_min =    Q_MIN(f)     + flow20*Q_MIN_A(f);
  if (c.fq_min < 0) c.fq_min = 0;

  c.q_lvmp =     Q_LVMP(f)     + flow20*Q_LVMP_A(f) + fhigh20*Q_LVMP_Z(f);
  c.q_sig_lvmp = Q_SIG_LVMP(f) + flow20*Q_SIG_LVMP_A(f) + fmid20*Q_SIG_LVMP_Z(f);
  if (c.q_sig_lvmp < 0.01) c.q_sig_lvmp = 0.01;

  c.icl_dist = ICL_DIST(f);
  //if (c.icl_dist > 1) c.icl_dist = 1;
  
  //Nuisance params
  c.obs_sm_sig     = OBS_SM_SIG(f)    + z_ceiling*OBS_SM_SIG_Z(f);
  if (c.obs_sm_sig > MAX_OBS_SM_SIG) c.obs_sm_sig = MAX_OBS_SM_SIG;
  c.obs_sfr_sig    = (scatter_tot < OBS_SCATTER_SFR_SF) ? 
    sqrt(OBS_SCATTER_SFR_SF*OBS_SCATTER_SFR_SF - scatter_tot*scatter_tot) : 0;
  c.obs_sm_offset  = MU(f)            + flow*MU_A(f);
  c.obs_sfr_offset = c.obs_sm_offset + KAPPA_A(f)*fsfr; //flow*KAPPA_A(f);

  c.obs_sm_offset_lin = pow(10, c.obs_sm_offset);
  c.obs_sfr_offset_lin = pow(10, c.obs_sfr_offset);
  c.obs_sm_scatter_corr =  scatter_correction(c.obs_sm_sig);
  c.obs_sfr_scatter_corr =  scatter_correction(c.obs_sfr_sig);
  c.obs_ssfr_scatter = sqrt((0.35)*c.obs_sm_sig*c.obs_sm_sig+c.obs_sfr_sig*c.obs_sfr_sig);
  c.obs_ssfr_scatter_corr = scatter_correction(c.obs_ssfr_scatter);
  
  c.sig_q = (c.obs_sfr_sig < OBS_SCATTER_SFR_Q) ? sqrt(OBS_SCATTER_SFR_Q*OBS_SCATTER_SFR_Q - c.obs_sfr_sig*c.obs_sfr_sig) : 0;

  //Dust
  double zm4 = z_ceiling;
  if (zm4 < 4) zm4 = 4;
  c.dust_m = DUST_M(f) + DUST_M_Z(f)*(zm4-4.0);
  c.dust_w = DUST_WIDTH(f);

  c.uv_dust_thresh = -20 -2.5*(c.dust_m - 9.2);
  c.uv_dust_slope = (0.23/c.dust_w);

  c.moustakas_constant = -0.49 + 1.07 * (z - 0.1);
  return c;
}
