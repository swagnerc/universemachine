import re

class ReadConfig:
    def trim(string):
        res = re.sub(r'^\s*[\'"]?', "", string);
        return re.sub(r'[\'"]?\s*$', "", res);

    def __init__(self, filename):
        self.data = {}
        file = open(filename, "r")
        for line in file:
            line = re.sub(r'#.*', "", line)
            res = re.match(r'([^=]+)=(.*)', line)
            if res==None:
                continue
            (key, value) = map(ReadConfig.trim, res.group(1,2))
            if (len(key)==0 or len(value)==0):
                continue
            self.data[key] = value
        file.close()

