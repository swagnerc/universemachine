#!/usr/bin/perl -w

my $z = 0.5;
my $translate_const = -0.49 + 1.07 * ($z-0.1);

while (<>) {
    my (@a) = split;
    next if (/^#/);
    my ($sm, $sfr) = @a[23,24];
    $sm = log($sm)/log(10);
    $sfr = log($sfr)/log(10);
    my $ssfr = $sfr - ($translate_const + 0.65*($sm-10));
    my $smb = int($sm*10+0.5)/10;
    $t{$smb}++;
    $nq{$smb}++ if ($ssfr < 0);
    $t++ if ($sm > 10.4 and $sm < 10.95);
    $nq++ if ($sm > 10.4 and $sm < 10.95 and $ssfr < 0);
}

for (sort {$a <=> $b} keys %t) {
    print "$_ ", ($nq{$_}/$t{$_}),"\n";
}

print $nq/$t," $t\n";
