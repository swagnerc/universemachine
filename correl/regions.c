#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include "../mt_rand.h"
#include "../check_syscalls.h"
#include "regions.h"

int64_t num_regions = 0;
int64_t num_super_regions = 0;
struct region *regions = NULL;
struct region *super_regions = NULL;


void rand_ra_dec(double *ra, double *dec) {
  *ra = dr250()*360.0;
  *dec = asin(2.0*dr250()-1.0);
  *dec *= 180.0/M_PI;
}

int64_t within_r(double ra, double dec) {
  int64_t i;
  for (i=0; i<num_super_regions; i++) {
    if (ra > super_regions[i].lims[0] && ra < super_regions[i].lims[2] &&
	dec > super_regions[i].lims[1] && dec < super_regions[i].lims[3]) break;
  }
  if (num_super_regions && i==num_super_regions) return 0;
  
  for (i=0; i<num_regions; i++)
    if (ra > regions[i].lims[0] && ra < regions[i].lims[2] &&
	dec > regions[i].lims[1] && dec < regions[i].lims[3]) return 1;
  return 0;
}

void rand_within_r(double *ra, double *dec) {
  do {
    rand_ra_dec(ra, dec);
  } while (!within_r(*ra, *dec));
}

int order_regions(const void *a, const void *b) {
  const struct region *c = a;
  const struct region *d = b;
  if (c->lims[0] < d->lims[0]) return -1;
  if (c->lims[0] > d->lims[0]) return 1;
  return 0;
}

double area_within_r(void) {
  int64_t i, j;
  double total = 0;
  struct region *subr = NULL;
  int64_t num_subr = 0;
  check_realloc_s(subr, sizeof(struct region), num_regions);

  qsort(regions, num_regions, sizeof(struct region), order_regions);

  for (i=-9000; i<9000; i++) {
    double dec = (i+0.5)/100.0;
    double weight = cos(dec*M_PI/180.0);
    num_subr = 0;
    for (j=0; j<num_regions; j++) {
      if (!(regions[j].lims[1] <= dec && regions[j].lims[3] > dec)) continue;
      subr[num_subr] = regions[j];
      num_subr++;
    }
    if (!num_subr) continue;

    struct region *cr = subr;
    for (j=1; j<num_subr; j++) {
        if (subr[j].lims[0] < cr->lims[2]) {
	  //merge regions
	  cr->lims[2] = subr[j].lims[2];
	  memset(subr+j, 0, sizeof(struct region));
        }
        else {
            cr = subr+j;
        }
    }

    double sum = 0;
    for (j=0; j<num_subr; j++) {
      if (!(subr[j].lims[1] || subr[j].lims[3])) continue;
      sum += subr[j].lims[2] - subr[j].lims[0];
    }
    total += sum * 0.01 * weight;
  }
  free(subr);
  return total;
}


void load_regions(char *filename, int64_t sr) {
  int64_t n;
  char buffer[1024];
  FILE *in = check_fopen(filename, "r");
  struct region r = {{0}};
  struct region **rr = (sr) ? &super_regions : &regions;
  int64_t *nr = (sr) ? &num_super_regions : &num_regions;

  while (fgets(buffer, 1024, in)) {
    n = sscanf(buffer, "%f %f %f %f", r.lims, r.lims+1, r.lims+2, r.lims+3);
    if (n<4) continue;
    check_realloc_every(rr[0], sizeof(struct region), nr[0], 1000);
    rr[0][nr[0]]=r;
    nr[0]++;
  }
  fclose(in);
}

