#ifndef _CORREL_H_
#define _CORREL_H_

#define MIN_Z    0.01

#include "correl_lib.h"

struct correl_point {
  float pos[3], r, inv_v;
  struct correl_galaxy *hg;
  int64_t div;
};


struct div_bounds {
  float bounds[6];
};

#define DIV_LOG2 4
#define TOTAL_DIV (2<<DIV_LOG2)

#define MIN_DIST -1.83250891270624 //-2

#ifdef KNN_MAIN
#define NUM_DIST_BINS 18
#else
#define NUM_DIST_BINS 15
#endif /*KNN_MAIN*/
#define NUM_PI_BINS 40

int64_t is_complete(float sm, float z);
double max_complete_z(float sm);
void load_hosts(char *filename);
void calc_pos3d(struct correl_point *c, double r);

#endif /* _CORREL_H_ */
