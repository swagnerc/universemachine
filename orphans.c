#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include "distance.h"
#include "masses.h"
#include "universe_time.h"
#include "orphans.h"
#include "universal_constants.h"

#undef Gc
#define Gc 4.39877036e-15  /* In Mpc^2 / Myr / Msun * km/s */
#define PERIODIC 1

extern double box_size; /* Automatically set; in Mpc/h */
extern double Om, Ol, h0; /* Hubble constant */

double dynamical_time(double a);
double vir_density(double a);

/* From http://arxiv.org/abs/1403.6827 */
double jiang_mass_loss_factor(double a, double m, double M) {
  return -0.93*pow(m/M, 0.07)/(dynamical_time(a)*M_PI_2);
}

double m_to_rvir(double m, double a) { //In kpc/h
  float mean_density = CRITICAL_DENSITY*Om; //(Msun/h) / (comoving Mpc/h)^3
  float vd = vir_density(a)*mean_density;
  return (cbrt(m / (4.0*M_PI*vd/3.0)) * 1000.0);
}

void evolve_orphan_halo(struct halo *h, struct halo *p1, struct halo *p2)
{
  int64_t i=0;
  float dt = (scale_to_years(p2->scale) - scale_to_years(p1->scale))/1.0e6 / (double)NUM_STEPS; //In Myr
  float av_a = (p1->scale+p2->scale)/2.0;
  float vir_conv_factor = pow(SOFTENING_LENGTH*1.0e-3, 2); //1e-3 is for converting Rvir from kpc/h to Mpc/h
  float max_dist = (PERIODIC) ? (box_size / 2.0) : (2.0*box_size);
  float time_start = scale_to_years(p1->scale);

  double acc[3] = {0};
  double mf1 = calculate_mass_factor(p1->mvir, p1->rvir, p1->rs);
  double mf2 = calculate_mass_factor(p2->mvir, p2->rvir, p2->rs);

  /* Mass loss depends only weakly on Mhost */
  double jmlf = 1e6*jiang_mass_loss_factor(av_a, h->mvir, 0.5*(p1->mvir+p2->mvir));

  for (i=0; i<NUM_STEPS+1; i++) {

    // Calculate the scale factor, hubble flow, and hubble drag for this timestep
    float time_years = time_start+dt*i;
    float a_t = years_to_scale(time_years);
    float H = h0*100.0 * sqrt(Om/(a_t*a_t*a_t)+Ol); //Hubble flow in km/s/(Mpc)
    float h_drag = - H * 1.02269032e-6; //km/s/(Mpc) to km/s/Myr / (km/s) = 1/Myr

    // Calculate time steps with unit coversion factored in
    // 1 km/s to comoving Mpc/Myr/h
    float vel_dt = dt * 1.02268944e-6 * h0 / a_t;
    // 1 km/s / Myr to comoving Mpc/Myr^2/h
    float acc_dt2 = (dt*dt / 2.0) * 1.02268944e-6 * h0 / a_t;
    float conv_const = a_t*a_t / h0 / h0;

    if (i != 0) { //If we're not at the first step.
      for (int64_t j=0; j<3; j++)
	h->vel[j] += acc[j]*dt*0.5;
    }
    
    if (i==NUM_STEPS) {
      h->scale = p2->scale;
      h->rvir = m_to_rvir(h->mvir, p2->scale);
      return; //Return if this is the final step.
    }
    
    struct halo p = *p1;
    double f = (double)(i+0.5) / (double)NUM_STEPS;
    
    for (int64_t j=0; j<3; j++)
      p.pos[j] += f*(p2->pos[j]-p1->pos[j]);
    
    double mf = mf1 + f*(mf2-mf1);
    double rs = cbrt((1.0-f)*pow(p1->rs, 3) + f*pow(p2->rs, 3));
    float inv_rs = 1000.0/rs;
    
    float r,m, dx, dy, dz, r3, rsoft2, accel;
    
#define DIST(a,b) { a = p.b - h->b; if (a > max_dist) a-=box_size; else if (a < -max_dist) a+=box_size;  }
    DIST(dx,pos[0]);
    DIST(dy,pos[1]);
    DIST(dz,pos[2]);
#undef DIST
    
    double vdotr = dx*h->vel[0] + dy*h->vel[1] + dz*h->vel[2];
    
    r = sqrtf(dx*dx + dy*dy + dz*dz); // in comoving Mpc/h
    
    m = mf*ff_cached(r*inv_rs); //In Msun
    
    //Apply force softening:
    rsoft2 = r*r + h->rvir*h->rvir*vir_conv_factor;
    r3 = rsoft2 * r * conv_const; //in (real Mpc)^2 * (comoving Mpc/h)
    accel = r ? Gc*m/r3 : 0; //In km/s / Myr / (comoving Mpc/h)
    
    acc[0] = accel*dx + h_drag*h->vel[0];      //In km/s / Myr
    acc[1] = accel*dy + h_drag*h->vel[1];
    acc[2] = accel*dz + h_drag*h->vel[2];
    
    if (vdotr < 0) // rhat points toward host halo
      h->mvir += 2.0*jmlf*dt*h->mvir;
    
    if (i != 0) { //If not at the first step
      for (int64_t j=0; j<3; j++) h->vel[j] += acc[j]*dt*0.5;
    }
    
    //Calculate new positions
    for (int64_t j=0; j<3; j++) {
      h->pos[j] += h->vel[j]*vel_dt + acc[j]*acc_dt2;
      if (PERIODIC) {
	if (h->pos[j] < 0) h->pos[j] += box_size;
	if (h->pos[j] > box_size) h->pos[j] -= box_size;
      }
    }
  }
}
    
