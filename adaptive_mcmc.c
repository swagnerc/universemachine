#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>
#include "adaptive_mcmc.h"
#include "sf_model.h"
#include "config_vars.h"
#include "jacobi_dim.h"
#include "mt_rand.h"
#include "check_syscalls.h"

void (*achi2_function)(struct sf_model_allz *) = NULL;
FILE *mcmc_output = NULL;
struct sf_model_allz starting_fit, fit_sum;

int64_t num_points = 0;

float inv_temperature = 1;
double identity_portion = 3e-5;

double cov_matrix[NUM_PARAMS*NUM_PARAMS];
double orth_matrix[NUM_PARAMS*NUM_PARAMS];
double eigenvalues[NUM_PARAMS];

int64_t mcmc_fit(int64_t length, int64_t run) {
  int64_t i, j, k, dups=0;
  double last_chi2, chi2, first_chi2;
  struct sf_model_allz cur_fit=starting_fit;

  struct sf_model_allz *trials = NULL;
  check_realloc_s(trials, sizeof(struct sf_model_allz), TOTAL_BOXES);
  for (k=0; k<TOTAL_BOXES; k++) trials[k] = starting_fit;
  achi2_function(trials);
  first_chi2 = last_chi2 = CHI2(trials[0]);

  for (i=1; i<length;) {
    if (i>=100 && (last_chi2==first_chi2 || (dups > i-7)) && !run) {
      for (j=0; j<NUM_PARAMS; j++) eigenvalues[j]*=0.5;
      clear_stats();
      num_points = dups = i=0;
      set_identity_portion();
      continue;
    }
    for (k=0; k<TOTAL_BOXES; k++) {
      trials[k] = cur_fit;
      random_step(trials+k);
    }
    achi2_function(trials);
    for (k=0; k<TOTAL_BOXES; k++) {
      i++;
      chi2 = CHI2(trials[k]);
      if (!isfinite(chi2) || chi2<0 || chi2 > 1e25 ||
	  (chi2>last_chi2 && dr250()>exp(0.5*inv_temperature*(last_chi2-chi2))))
	{
	  dups++;
	  chi2 = last_chi2;
	}
      else {
	cur_fit = trials[k];
	k=TOTAL_BOXES;
      }
      add_to_stats(&cur_fit);
      num_points++;
      last_chi2 = chi2;
      for (j=0; j<NUM_PARAMS+1; j++) fprintf(mcmc_output, "%.10f ", cur_fit.params[j]);
      fprintf(mcmc_output, "%f\n", CHI2(cur_fit));
    }
    if (run > 0) stats_to_step(num_points);
    fflush(mcmc_output);
  }
  starting_fit = cur_fit;
  return dups;
}

void set_identity_portion(void) {
  int64_t i;
  for (i=0; i<NUM_PARAMS; i++) {
    if ((identity_portion > 0.05*fabs(eigenvalues[i])) &&
	fabs(eigenvalues[i])>0)
      identity_portion = fabs(eigenvalues[i]*0.05);
  }
  fprintf(stderr, "#Set Identity coefficient to %.3e\n", identity_portion);
}

void adaptive_mcmc(double *params, double *steps, void (*c2f)(struct sf_model_allz *), FILE *output)
{
  int64_t i;
  float temps[5] = {0.4, 0.2, 0.3, 0.5, 0.8};
  int64_t num_temps = 5;
  r250_init(87L);
  
  mcmc_output = output;
  achi2_function = c2f;

  memcpy(starting_fit.params, params, sizeof(double)*NUM_PARAMS);
  memcpy(eigenvalues, steps, sizeof(double)*NUM_PARAMS);
  assert_model(&starting_fit);
  achi2_function(&starting_fit);
  init_orth_matrix();
  fprintf(output, "Initial Chi2 fit: %f\n", CHI2(starting_fit));

  clear_stats();
  set_identity_portion();
  for (i=0; i<num_temps; i++) {
    inv_temperature = temps[i];
    fprintf(output, "#Temp now: %f\n", 1.0/inv_temperature);
    mcmc_fit(BURN_IN_LENGTH*temps[i], 0);
    stats_to_step(num_points);
    if (i<num_temps-1) clear_stats();
  }
  inv_temperature = 1;
  fprintf(output, "#Temp now: %f\n", 1.0/inv_temperature);
  set_identity_portion();
  mcmc_fit(MCMC_LENGTH/4.0, 1);
  set_identity_portion();
  clear_stats();
  mcmc_fit(MCMC_LENGTH/8.0, 1);
  print_orth_matrix(1.0);
  mcmc_fit(MCMC_LENGTH, 2);
}


void init_orth_matrix(void) {
  int i,j;
  struct sf_model_allz address;
  for (i=0; i<NUM_PARAMS; i++)
    for (j=0; j<NUM_PARAMS; j++)
      orth_matrix[i*NUM_PARAMS+j] = (i==j) ? 1 : 0;

  if (!NO_SYSTEMATICS) {
    i = &(MU(address)) - address.params;
    j = &(EFF_0(address)) - address.params;
    orth_matrix[i*NUM_PARAMS+j] = -1; //Build in anti-correlation between MU and EFF
  }
}

void print_orth_matrix(float temp) {
  FILE *output;
  char buffer[1024];
  int i,j;
  sprintf(buffer, "%s/orth_matrix_t%f.dat", OUTBASE, temp);
  output = check_fopen(buffer, "w");
  for (i=0; i<NUM_PARAMS; i++) {
    fprintf(output, "Eig[%d]: %f\n", i, eigenvalues[i]);
  }
  fprintf(output, "\n");
  for (i=0; i<NUM_PARAMS; i++) {
    for (j=0; j<NUM_PARAMS; j++)
      fprintf(output, "%+.4f ", orth_matrix[i*NUM_PARAMS+j]);
    fprintf(output, "\n");
  }
  fclose(output);
}

void clear_stats(void) {
  int i, j;
  memset(&fit_sum, 0, sizeof(struct sf_model_allz));
  for (i=0; i<NUM_PARAMS; i++) {
    fit_sum.params[i] = 0;
    for (j=0; j<NUM_PARAMS; j++) {
      cov_matrix[i*NUM_PARAMS+j] = 0;
    }
  }
  num_points = 0;
}

void add_to_stats(struct sf_model_allz *a) {
  int i,j;
  for (i=0; i<NUM_PARAMS; i++) {
    fit_sum.params[i]+=a->params[i];
    for (j=0; j<NUM_PARAMS; j++) {
      cov_matrix[i*NUM_PARAMS+j] += a->params[i]*a->params[j];
    }
  }
}

void stats_to_step(int64_t num_stats) {
  double mul = 1.0/(double)num_stats;
  double sd_mul = sqrt(2.4*2.4/((double)NUM_PARAMS));
  int i,j;
  double input_matrix[NUM_PARAMS*NUM_PARAMS];

  for (i=0; i<NUM_PARAMS; i++)
    for (j=0; j<NUM_PARAMS; j++)
      input_matrix[i*NUM_PARAMS+j] = mul*cov_matrix[i*NUM_PARAMS+j] -
	mul*mul*fit_sum.params[i]*fit_sum.params[j];

  for (i=0; i<NUM_PARAMS; i++) input_matrix[i*NUM_PARAMS+i] += identity_portion;

  jacobi_decompose_dim(input_matrix, eigenvalues, orth_matrix, NUM_PARAMS);
  for (i=0; i<NUM_PARAMS; i++) {
    if (eigenvalues[i] <=0 ) eigenvalues[i] = 0;
    eigenvalues[i] = sqrt(eigenvalues[i])*sd_mul;
  }
  //clear_stats();
}


void random_step(struct sf_model_allz *a) {
  int i,j,num_params=6;
  for (i=0; i<num_params; i++) { //NUM_PARAMS/2; i++) {
    j = rand()%NUM_PARAMS;
    vector_madd_dim(a->params, normal_random(0,eigenvalues[j]), orth_matrix+j*NUM_PARAMS, NUM_PARAMS);
  }
  assert_model(a);
}

void assert_model(struct sf_model_allz *a) {
  if (NO_SYSTEMATICS) assert_model_no_systematics(a);
  else assert_model_default(a);
}
