#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"


int sort_floats(const void *a, const void *b) {
  const float *c = a;
  const float *d = b;
  if (*c<*d) return -1;
  if (*c>*d) return 1;
  return 0;
}

double uv_to_sfr(double uv) {
  return 0.16*pow(10,((-17.0-uv)/2.5));
}

double integrate_power(double nd0, double l0, double lum_range, double slope) {
  double tot = 0;
  int64_t i;
  for (i=1; i<(lum_range*20); i++) {
    double mag = l0 + (double)i/(20.0);
    double sfr = uv_to_sfr(mag);
    double nd = nd0 * pow(10, (-1.0*(slope+1.0)*(mag-l0)/2.5));
    tot += sfr*nd/20.0;
  }
  return tot;
}

int main(int argc, char **argv) {
  int64_t i, j;
  char buffer[1024];
  if (argc < 3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg  /paths/to/model_outputs ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }  
  
  do_config(argv[1]);
  init_cosmology(Om, Ol, h0);
  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] POSTPROCESSING_MODE must be enabled to extract smhm fits.\n");
    exit(EXIT_FAILURE);
  }

  struct mcmc_data *m = load_mcmc_data(argv[2]);

  snprintf(buffer, 1024, "%s/post_cmfs/", OUTBASE);
  mkdir(buffer, 0755);
  
  float *cmfs = NULL;
  float *cuvfs = NULL;
  float *nsm_gt_z = NULL;
  float *nuv_gt_z = NULL;
  int64_t num_files = argc-2;

  double volume = pow(BOX_SIZE/h0, 3.0);
  for (i=2; i<argc; i++) {
    struct mcmc_data *m = load_mcmc_data(argv[i]);
    if (!cmfs) {
      check_calloc_s(cmfs, sizeof(float), m->num_scales*num_files*SM_NBINS);
      check_calloc_s(cuvfs, sizeof(float), m->num_scales*num_files*UV_NBINS);
      check_calloc_s(nsm_gt_z, sizeof(float), m->num_scales*num_files*SM_NBINS);
      check_calloc_s(nuv_gt_z, sizeof(float), m->num_scales*num_files*UV_NBINS);
    }

    for (j=0; j<m->num_scales; j++) {
      int64_t k=0;
      double total = 0;
      for (k=SM_NBINS-1; k>=0; k--) {
	total += m->smf[j*SM_NBINS+k];
	cmfs[j*num_files*SM_NBINS + k*num_files+i-2] = total/volume;
      }
      total=0;
      for (k=0; k<UV_NBINS; k++) {
	total += m->uvlf[j*UV_NBINS+k];
	cuvfs[j*num_files*UV_NBINS + k*num_files+i-2] = total/volume;
      }
    }
    for (j=0; j<m->num_scales; j++) {
      int64_t k=0;
      double a1 = (j>0) ? (0.5*(m->scales[j]+m->scales[j-1])) : 0.001;
      double a2 = (j<m->num_scales-1) ? (0.5*(m->scales[j]+m->scales[j+1])) : m->scales[j];
      double v_per_sq_arcmin = (comoving_volume(1.0/a1 - 1.0) - comoving_volume(1.0/a2-1.0))/
	(4.0*(180.0*180.0/M_PI)*3600.0);
      for (k=0; k<SM_NBINS; k++) {
	int64_t bin = j*num_files*SM_NBINS + k*num_files+i-2;
	if (j>0) nsm_gt_z[bin] = nsm_gt_z[bin - num_files*SM_NBINS];
	else nsm_gt_z[bin] = 0;
	nsm_gt_z[bin] += cmfs[bin]*v_per_sq_arcmin;
      }
      for (k=0; k<UV_NBINS; k++) {
	int64_t bin = j*num_files*UV_NBINS + k*num_files+i-2;
	if (j>0) nuv_gt_z[bin] = nuv_gt_z[bin - num_files*UV_NBINS];
	else nuv_gt_z[bin] = 0;
	nuv_gt_z[bin] += cuvfs[bin]*v_per_sq_arcmin;
      }
    }
  }

  for (j=0; j<m->num_scales; j++) {
    snprintf(buffer, 1024, "%s/post_cmfs/csmf_a%f.dat", OUTBASE, m->scales[j]);
    FILE *out_smf = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "%s/post_cmfs/cuvlf_a%f.dat", OUTBASE, m->scales[j]);
    FILE *out_uvlf = check_fopen(buffer, "w");

    double a2 = (j<m->num_scales-1) ? (0.5*(m->scales[j]+m->scales[j+1])) : m->scales[j];
    snprintf(buffer, 1024, "%s/post_cmfs/nsm_gt_z_a%f.dat", OUTBASE, a2);
    FILE *out_nsm = check_fopen(buffer, "w");
    snprintf(buffer, 1024, "%s/post_cmfs/nuv_gt_z_a%f.dat", OUTBASE, a2);
    FILE *out_nuv = check_fopen(buffer, "w");

    fprintf(out_smf, "#Observed stellar masses in Msun\n");
    fprintf(out_smf, "#Number densities in comoving Mpc^-3.\n");
    fprintf(out_smf, "#Log10(SM) Cumulative_Number_Density Err+ Err- Err+(2sig) Err-(2sig)\n");

    fprintf(out_nsm, "#Observed stellar masses in Msun\n");
    fprintf(out_nsm, "#Number densities of galaxies with z>%f in arcmin^-2.\n", 1.0/a2-1.0);
    fprintf(out_nsm, "#Log10(SM) Cumulative_Number_Density Err+ Err- Err+(2sig) Err-(2sig)\n");

    fprintf(out_uvlf, "#Observed UV luminosities (including dust) are M_1500 [AB].\n");
    fprintf(out_uvlf, "#Number densities in comoving Mpc^-3.\n");
    fprintf(out_uvlf, "#M_1500[AB] Cumulative_Number_Density Err+ Err- Err+(2sig) Err-(2sig)\n");

    fprintf(out_nuv, "#Observed UV luminosities (including dust) are M_1500 [AB].\n");
    fprintf(out_nuv, "#Number densities of galaxies with z>%f in arcmin^-2.\n", 1.0/a2-1.0);
    fprintf(out_nuv, "#M_1500[AB] Cumulative_Number_Density Err+ Err- Err+(2sig) Err-(2sig)\n");

    
    for (int64_t k=0; k<SM_NBINS; k++) {
      int64_t bin = j*num_files*SM_NBINS + k*num_files;
      //int64_t med = bin+0.5*num_files;
      int64_t dn = bin+0.5*(1.0-ONE_SIGMA)*num_files;
      int64_t up = bin+0.5*(1.0+ONE_SIGMA)*num_files;
      int64_t dn2 = bin+0.5*(1.0-TWO_SIGMA)*num_files;
      int64_t up2 = bin+0.5*(1.0+TWO_SIGMA)*num_files;
      double sm = SM_MIN+((double)k/(double)SM_BPDEX);
      double bf = cmfs[bin];
      qsort(cmfs+bin, num_files, sizeof(float), sort_floats);
      fprintf(out_smf, "%g %g %g %g %g %g\n", sm, bf, cmfs[up]-bf, bf-cmfs[dn], cmfs[up2]-bf, bf-cmfs[dn2]);
      bf = nsm_gt_z[bin];
      qsort(nsm_gt_z+bin, num_files, sizeof(float), sort_floats);
      fprintf(out_nsm, "%g %g %g %g %g %g\n", sm, bf, nsm_gt_z[up]-bf, bf-nsm_gt_z[dn], nsm_gt_z[up2]-bf, bf-nsm_gt_z[dn2]);
    }

    for (int64_t k=0; k<UV_NBINS; k++) {
      int64_t bin = j*num_files*UV_NBINS + k*num_files;
      //int64_t med = bin+0.5*num_files;
      int64_t dn = bin+0.5*(1.0-ONE_SIGMA)*num_files;
      int64_t up = bin+0.5*(1.0+ONE_SIGMA)*num_files;
      int64_t dn2 = bin+0.5*(1.0-TWO_SIGMA)*num_files;
      int64_t up2 = bin+0.5*(1.0+TWO_SIGMA)*num_files;
      double uv = UV_MIN+((double)(k+1.0)/(double)UV_BPMAG);
      double bf = cuvfs[bin];
      qsort(cuvfs+bin, num_files, sizeof(float), sort_floats);
      fprintf(out_uvlf, "%g %g %g %g %g %g\n", uv, bf, cuvfs[up]-bf, bf-cuvfs[dn], cuvfs[up2]-bf, bf-cuvfs[dn2]);
      bf = nuv_gt_z[bin];
      qsort(nuv_gt_z+bin, num_files, sizeof(float), sort_floats);
      fprintf(out_nuv, "%g %g %g %g %g %g\n", uv, bf, nuv_gt_z[up]-bf, bf-nuv_gt_z[dn], nuv_gt_z[up2]-bf, bf-nuv_gt_z[dn2]);
    }

    fclose(out_smf);
    fclose(out_uvlf);
    fclose(out_nsm);
    fclose(out_nuv);
  }
  return 0;
}
