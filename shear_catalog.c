#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <sys/mman.h>
#include <gsl/gsl_spline.h>
#include "make_sf_catalog.h"
#include "check_syscalls.h"
#include "stringparse.h"
#include "inthash.h"

#define BPDEX 8
#define MIN_R -2
#define WL_NUM_BINS 20
#define MASS_BINS 10

struct shear {
  int64_t id;
  int32_t bins[WL_NUM_BINS];
};
struct inthash *idx = NULL;
struct shear *sh = NULL;
int64_t num_sh = 0;

int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s sfr_catalog shear_catalog pmass\n", argv[0]);
    exit(1);
  }

  float pmass = 1.55e8/0.68*pow(2048, 3) / 100e6;
  if (argc > 3) pmass = atof(argv[3]);

  char buffer[1024];
  FILE *in = check_fopen(argv[2], "r");
  struct shear s;
  int64_t i, j;
  void *data[WL_NUM_BINS+4];
  enum parsetype pt[WL_NUM_BINS+4];
  data[0] = &s.id;
  pt[0] = PARSE_INT64;
  data[1] = data[2] = data[3] = NULL;
  pt[1] = pt[2] = pt[3] = PARSE_SKIP;
  for (i=0; i<WL_NUM_BINS; i++) {
    data[i+4] = s.bins+i;
    pt[i+4] = PARSE_INT32;
  }

  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    if (stringparse(buffer, data, pt, WL_NUM_BINS+4)!=(WL_NUM_BINS+4)) continue;
    check_realloc_every(sh, sizeof(struct shear), num_sh, 1000);
    sh[num_sh] = s;
    num_sh++;
  }
  fclose(in);

  struct inthash *idx = new_inthash();
  ih_prealloc(idx, num_sh);
  for (i=0; i<num_sh; i++) ih_setint64(idx, sh[i].id, i);

  int64_t length = 0;
  struct catalog_halo *ch = check_mmap_file(argv[1], 'r', &length);
  mlock(ch, length);
  assert(!(length % sizeof(struct catalog_halo)));
  int64_t num_halos = length / sizeof(struct catalog_halo);
  int64_t total_shear[MASS_BINS][3][WL_NUM_BINS];
  int64_t counts[MASS_BINS][3];
  float lgm_min[MASS_BINS] = {9.8, 10.2, 10.6, 9.72, 10.03, 10.33, 10.63, 10.93, 11.23, 11.54};
  float lgm_max[MASS_BINS] = {14, 14, 14, 10.03, 10.33, 10.63, 10.93, 11.23, 11.54, 11.84};
  float masses_min[MASS_BINS], masses_max[MASS_BINS];

  int64_t k, q;
  for (i=0; i<MASS_BINS; i++)
    for (j=0; j<3; j++) {
      for (k=0; k<WL_NUM_BINS; k++)
	total_shear[i][j][k] = 0;
      counts[i][j] = 0;
    }
  for (i=0; i<MASS_BINS; i++) {
    masses_min[i] = pow(10, lgm_min[i]);
    masses_max[i] = pow(10, lgm_max[i]);
  }

  for (i=0; i<num_halos; i++) {
    //    if (ch[i].obs_sm < masses[0]) continue;
    for (j=0; j<MASS_BINS; j++) {
      q = (ch[i].obs_sm*1e-11 > ch[i].obs_sfr) ? 2 : 1;
      if (j>2) {
	q = (pow(ch[i].obs_sm*(1e-11), 0.8) > ch[i].obs_sfr) ? 2 : 1;
      }
      int64_t offset = ih_getint64(idx, ch[i].id);
      assert(offset != IH_INVALID);
      struct shear *s = sh+offset;
      if (ch[i].obs_sm > masses_min[j] && ch[i].obs_sm < masses_max[j]) {
	counts[j][0]++;
	counts[j][q]++;
	for (k=0; k<WL_NUM_BINS; k++) {
	  total_shear[j][q][k] += s->bins[k];
	  total_shear[j][0][k] += s->bins[k];
	  //if (!k && s->bins[k]>1000) {
	    //	    printf("#WTF?? ID: %"PRId64"\n", ch[i].id);
	  //}
	}
      }
    }
  }
  munmap(ch, length);

  double r[WL_NUM_BINS];
  double t_r[WL_NUM_BINS];
  double dens[MASS_BINS][3][WL_NUM_BINS];
  double total_dens[MASS_BINS][3][WL_NUM_BINS];
  gsl_spline *g_dens[MASS_BINS][3];
  gsl_spline *g_tdens[MASS_BINS][3];

  for (j=0; j<MASS_BINS; j++) {
    for (q=0; q<3; q++) {
      printf("#%f - %f %"PRId64" %"PRId64, lgm_min[j], lgm_max[j], q, counts[j][q]);
      int64_t total = 0;
      for (k=0; k<WL_NUM_BINS; k++) {
	double r2 = pow(10, MIN_R+k/(double)BPDEX)/0.68;
	double r1 = pow(10, MIN_R+(k-1)/(double)BPDEX)/0.68;
	if (k==0) r1 = 0;
	double a = M_PI*(r2*r2-r1*r1);
	double av_r = sqrt(r1*r1+ 0.5*(r2*r2-r1*r1));
	double total_a = M_PI*r2*r2;
	r[k] = av_r;
	t_r[k] = r2;
	dens[j][q][k] = total_shear[j][q][k]/(double)(counts[j][q]*a);
	total += total_shear[j][q][k];
	total_dens[j][q][k] = total/(double)(counts[j][q]*total_a);
	//printf(" %f(%f) %f(%f)", dens[j][q][k], total_shear[j][q][k]/(double)(counts[j][q]), total_dens[j][q][k], total/(double)(counts[j][q]));
      }
      printf("\n");
      g_dens[j][q] = gsl_spline_alloc(gsl_interp_akima, WL_NUM_BINS);
      g_tdens[j][q] = gsl_spline_alloc(gsl_interp_akima, WL_NUM_BINS);
      gsl_spline_init(g_dens[j][q], r, dens[j][q], WL_NUM_BINS);
      gsl_spline_init(g_tdens[j][q], t_r, total_dens[j][q], WL_NUM_BINS);
    }
  }

  gsl_interp_accel ga = {0};
  printf("#R");
  for (j=0; j<MASS_BINS; j++)
    for (q=0; q<3; q++)
      printf(" SM(%g-%g,%"PRId64")", lgm_min[j], lgm_max[j], q);
  printf("\n");

  double norm = 1e-12;
  for (k=1; k<WL_NUM_BINS; k++) {
    double r2 = pow(10, MIN_R+k/(double)BPDEX)/0.68;
    double r1 = pow(10, MIN_R+(k-1)/(double)BPDEX)/0.68;
    if (k==0) r1 = 0;
    double av_r = sqrt(r1*r1+ 0.5*(r2*r2-r1*r1));
    printf("%f", av_r);
    for (j=0; j<MASS_BINS; j++) {
      for (q=0; q<3; q++) {
	double r_dens = gsl_spline_eval(g_dens[j][q], av_r, &ga);
	double enc_dens = gsl_spline_eval(g_tdens[j][q], av_r, &ga);
	printf(" %e", norm*pmass*(enc_dens-r_dens));
      }
    }
    printf("\n");
  }
  return 0;
}
