#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sf_model.h"
#include "io_helpers.h"
#include "check_syscalls.h"
#include "jacobi_dim.h"
#include "mt_rand.h"

#define POINTS_TO_GEN 100000



int main(int argc, char **argv) {
  r250_init(87L);
  FILE *in;
  if (argc < 2) in = stdin;
  else in = check_fopen(argv[1], "r");
  
  int64_t prec_matrix_counts[NUM_PARAMS][NUM_PARAMS] = {{0}};
  double prec_matrix[NUM_PARAMS*NUM_PARAMS] = {0};
  double orth_matrix[NUM_PARAMS*NUM_PARAMS] = {0};
  double eigenvalues[NUM_PARAMS] = {0};
  double sd[NUM_PARAMS] = {0};

  prec_matrix[26*NUM_PARAMS+26] = 1;
  prec_matrix[36*NUM_PARAMS+36] = 1;
  
  char buffer[2048];
  struct sf_model_allz m, bestfit, d;
  int64_t n=0, i=0, j=0, k=0;
  while (fgets(buffer, 2048, in)) {
    read_params(buffer, m.params, NUM_PARAMS+2);
    if (n==0) { bestfit = m; n++; continue; }
    i=j=-1;
    double chi2_diff = CHI2(m) - CHI2(bestfit);
    if (chi2_diff < 0) {
      fprintf(stderr, "[Error]: bestfit not at minimum...\n");
      continue;
    }
    for (k=0; k<NUM_PARAMS; k++) {
      d.params[k] = m.params[k]-bestfit.params[k];
      if (d.params[k]!=0) {
	if (i==-1) i=k;
	else if (j==-1) j=k;
	else {
	  fprintf(stderr, "Error in covariance matrix... %"PRId64" %"PRId64" %"PRId64" %s\n", i, j, k, buffer);
	}
      }
    }
    if (i==j && j==-1) continue;
    double pmatrix = 0;
    if (j==-1) { // d^2_i
      double ddx = chi2_diff / d.params[i];
      double d2dx2 = ddx / (0.5*d.params[i]);
      pmatrix = d2dx2 / 2.0;
      j=i;
    } else {
      if (!prec_matrix_counts[i][i] || !prec_matrix_counts[j][j]) {
	fprintf(stderr, "%"PRId64" %"PRId64" %f %f Input order of points not correct!\n%s", i, j, d.params[i], d.params[j], buffer);
	exit(EXIT_FAILURE);
      }
      double exp_chi2_dx = prec_matrix[i*NUM_PARAMS+i]*d.params[i]*d.params[i];
      double exp_chi2_dy = prec_matrix[j*NUM_PARAMS+j]*d.params[j]*d.params[j];
      double delta_c2 = chi2_diff - exp_chi2_dx - exp_chi2_dy;
      pmatrix = delta_c2 / (d.params[i]*d.params[j]*2.0);
    }

    double diff = pmatrix-prec_matrix[i*NUM_PARAMS+j];
    if (prec_matrix_counts[i][j]) {
      fprintf(stderr, "%"PRId64" %"PRId64" Old: %f; New: %f\n", i, j, prec_matrix[i*NUM_PARAMS+j], pmatrix);
      if (prec_matrix[i*NUM_PARAMS+j]*pmatrix<0) {
	prec_matrix[i*NUM_PARAMS+j] = 0;
	diff = 0;
      }
    }
    prec_matrix[i*NUM_PARAMS+j] += diff/(1.0+(double)prec_matrix_counts[i][j]);
    prec_matrix_counts[i][j]++;
    prec_matrix[j*NUM_PARAMS+i] = prec_matrix[i*NUM_PARAMS+j];
  }


  jacobi_decompose_dim(prec_matrix, eigenvalues, orth_matrix, NUM_PARAMS);
  for (k=0; k<NUM_PARAMS; k++) {
    if (eigenvalues[k] < 0)
      eigenvalues[k] = fabs(eigenvalues[k])*100.0;
    sd[k] = 1.0/sqrt(eigenvalues[k]);
  }

  for (k=0; k<POINTS_TO_GEN; k++) {
    m = bestfit;
    double chi2 = 0;
    for (i=0; i<NUM_PARAMS; i++) {
      double v = normal_random(0, 1);
      vector_madd_dim(m.params, v*sd[i], orth_matrix+(i*NUM_PARAMS), NUM_PARAMS);
      chi2 += v*v;
    }
    m.params[26] = bestfit.params[26];
    m.params[36] = bestfit.params[36];
    if (test_invalid(m)) {
      k--;
      continue;
    }
    for (i=0; i<NUM_PARAMS; i++)
      printf("%12lf ", m.params[i]);
    printf("%12f\n", CHI2(m)+chi2);
  }  
  return 0;
}
