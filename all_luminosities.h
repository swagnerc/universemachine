#ifdef _LUMINOSITIES_H_
#error "luminosities.h and all_luminosities.h cannot be loaded simultaneously!"
#endif

#ifndef _ALL_LUMINOSITIES_H_
#define _ALL_LUMINOSITIES_H_


//#define USE_GSL
#define CALZETTI_DUST
#define METALLICITY_LOWER_LIMIT -1.5

#ifndef CALZETTI_DUST
#define L_NUM_AGES 188
#else
#define L_NUM_AGES 94
#endif /* CALZETTI_DUST */

#define L_NUM_ZS 22
#define L_NUM_REDSHIFTS 301
#define L_NUM_BINS_PER_REDSHIFT 20
#define LUM_TYPES 28
#define DUST_TYPES 2

enum lum_types {
  L_sloan_g=0,
  L_sloan_r,
  L_sloan_i,
  L_cousins_i,
  L_f160w,
  L_f435w,
  L_f606w,
  L_f775w,
  L_f814w,
  L_f850lp,
  L_2mass_j,
  L_2mass_h,
  L_2mass_k,
  L_fors_r,
  L_m1500,
  L_jwst_f200w,
  L_jwst_f277w,
  L_jwst_f356w,
  L_jwst_f444w,
  L_f105w,
  L_f125w,
  L_Jn_V,
  L_Jn_U,
  L_galex_nuv,
  L_f140w,
  L_f098m,
  L_irac1,
  L_irac2,
};

extern char *lum_names[LUM_TYPES];

struct luminosities {
  double unused;
  double ages[L_NUM_AGES];
  double Zs[L_NUM_ZS];
  double redshifts[L_NUM_REDSHIFTS];
  double lum[LUM_TYPES][L_NUM_REDSHIFTS][L_NUM_ZS][L_NUM_AGES];
};

extern struct luminosities *tlum;

void load_luminosities(char *directory);
void gen_lum_lookup(float **lum_lookup, float *scales, int64_t num_scales, enum lum_types t, float z_src);
float lookup_lum(float *lum_lookup, float Z, int64_t n, int64_t i, int64_t dust);
float lum_of_sfh(float *lum_lookup, float *sfh, float *scales, int64_t n, float dust);
float lum4_of_sfh(float *ll, float *ll2, float *ll3, float *ll4, float *sfh, float *scales, int64_t n, float dust, float *uv, float *vj);
float fake_lum4_of_sfh(float *ll, float *ll2, float *ll3, float *ll4, float *sfh, float *scales, int64_t n, float dust, float *uv, float *vj);

double lum_at_age_Z(struct luminosities *lum, enum lum_types t, float age1, float age2, float Z, float z_src, int64_t dust);
float metallicity(float sm, float z);
void alloc_metallicity_cache(float *scales, int64_t n);
float metallicity_cache(float sm, float z, int64_t n);
double metallicity_moustakas(float sm, float z);
double metallicity_mannucci(float sm, float sfr);
double distance_factor(double z_src, double z_obs);
void free_luminosities(void);
void rescale_dust(float **lum_lookup, int64_t num_scales, double mag_ratio);

#endif /* _ALL_LUMINOSITIES_H_ */
