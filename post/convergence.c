#include <stdio.h>
#include <stdlib.h>
#include "../io_helpers.h"
#include "../check_syscalls.h"
#include "../sf_model.h"
#include "../sampler.h"
#include "../stringparse.h"

int main(int argc, char **argv) {
  int64_t i;
  double params[NUM_PARAMS+1];
  double *param_lists[NUM_PARAMS+1] = {0};
  char buffer[2048];
  int64_t ns = 0;
  
  if (argc < 3) {
    fprintf(stderr, "Usage: %s mcmc_steps.dat nwalkers\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  SHORT_PARSETYPE;
  void *data[NUM_PARAMS+1];
  enum parsetype types[NUM_PARAMS+1];
  for (i=0; i<NUM_PARAMS+1; i++) { data[i] = params+i; types[i] = F64; }
  
  int64_t nw = atol(argv[2]);
  FILE *in = check_fopen(argv[1], "r");
  while (fgets(buffer, 2048, in)) {
    stringparse(buffer, data, types, NUM_PARAMS+1);
    if (!(ns%1000)) {
      for (i=0; i<NUM_PARAMS+1; i++)
	check_realloc_s(param_lists[i], sizeof(double), (ns+1000));
    }
    for (i=0; i<NUM_PARAMS+1; i++) param_lists[i][ns] = params[i];
    ns++;
  }
  fclose(in);

  for (i=0; i<NUM_PARAMS+1; i++)
    print_autocorrelation(param_lists[i], nw, ns, stdout);
  return 0;
}
