#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stats.h"
#include "mt_rand.h"

double box_size = 250/0.677;
void print_nd(struct binstats *bs, double mul);

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<2) {
    printf("Usage: %s sfr_list.bin [box_size]\n", argv[0]);
    exit(1);
  }
  
  if (argc > 2) box_size = atof(argv[2]);

  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  struct binstats *bs_obs = init_binstats(-23, -15, 2, 1, 0);
  struct binstats *bs_real = init_binstats(-23, -15, 2, 1, 0);
  struct binstats *bs_real_lsm = init_binstats(-23, -15, 4, 1, 0);
  struct binstats *bs_obs_lsm = init_binstats(-23, -15, 4, 1, 0);
  
  struct binstats *smf_obs = init_binstats(7, 13, 20, 0, 0);
  struct binstats *smf_real = init_binstats(7, 13, 20, 0, 0);
  //struct binstats *smf_otest = init_binstats(7, 13, 20, 0, 0);
  //struct binstats *smf_ons = init_binstats(7, 13, 20, 0, 0);
  struct binstats *uv_obs = init_binstats(-23, -15, 4, 0, 0);

  
  //int64_t j;
  r250_init(87L);
  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    if (tp->flags & IGNORE_FLAG) continue;
    add_to_binstats(bs_obs, tp->obs_uv, tp->obs_sm);
    add_to_binstats(bs_real, tp->obs_uv, tp->sm);
    double sm = (tp->sm > 1) ? log10(tp->sm) : 0;
    double sm_obs = (tp->obs_sm > 1) ? log10(tp->obs_sm) : 0;
    add_to_binstats(smf_real, sm, 1);
    add_to_binstats(smf_obs, sm_obs, 1);
    //add_to_binstats(smf_ons, sm-0.126923, 1);
    add_to_binstats(uv_obs, tp->obs_uv, 1);
    add_to_binstats(bs_real_lsm, tp->obs_uv, sm);
    add_to_binstats(bs_obs_lsm, tp->obs_uv, sm_obs);
    /*for (j=0; j<100; j++) {
      sm_obs = sm+normal_random(-0.126923,0.077511);
      add_to_binstats(smf_otest, sm_obs, 1);
      }*/
  }
  munmap(p, new_length);

  printf("#Real stats:\n");
  print_medians(bs_real, stdout);
  printf("\n#Obs stats:\n");
  print_medians(bs_obs, stdout);

  printf("#Real stats (log):\n");
  print_medians(bs_real_lsm, stdout);
  printf("\n#Obs stats (log):\n");
  print_medians(bs_obs_lsm, stdout);

  double mul = 20.0/pow(box_size, 3.0);
  printf("\n#Real SMF:\n");
  print_nd(smf_real, mul);

  printf("\n#Obs SMF:\n");
  print_nd(smf_obs, mul);

  //printf("\n#Obs SMF w/o scatter:\n");
  //print_nd(smf_ons, mul);

  //printf("\n#Resampled Obs SMF:\n");
  //print_nd(smf_otest, mul/100.0);

  mul = 4.0/pow(box_size, 3.0);
  printf("\n#Obs UVLF:\n");
  print_nd(uv_obs, mul);

  return 0;
}

  
void print_nd(struct binstats *bs, double mul) {
  int64_t i;
  for (i=0; i<bs->num_bins; i++) {
    if (!(bs->counts[i]>0)) continue;
    printf("%f %e %"PRId64"\n", bs->bin_start+(i+0.5)/bs->bpunit, ((double)bs->counts[i])*mul, bs->counts[i]);
  }
}
