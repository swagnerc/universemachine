#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define check_y(yt,j,j2,n,ix)		  \
      if (((y[j]<yt) && (y[j2] > yt)) ||  \
	  ((y[j]>yt) && (y[j2] < yt))) {  \
	f = (yt-y[j])/(y[j2]-y[j]);       \
	ix[n] = x[j] + f*(x[j2]-x[j]);    \
	n++;                              \
      }

#define check_z(zt,j,j2,n,ix,iy)     \
    if ((z[j]<zt && z[j2] > zt) ||   \
	(z[j]>zt && z[j2] < zt)) {   \
      f = (zt-z[j])/(z[j2]-z[j]);    \
      ix[n] = x[j] + f*(x[j2]-x[j]); \
      iy[n] = y[j] + f*(y[j2]-y[j]); \
      n++;                           \
    }

void default_cb(int x, int y, int z, void *extra) {
  printf("%f %f %f\n", x+drand48(), y+drand48(), z+drand48());
}

static inline void min_max(float x[], int n, float step, int *min, int *max) {
  int i;
  float minf = x[0], maxf = x[0];
  for (i=0; i<n; i++) {
    if (maxf < x[i]) maxf = x[i];
    if (minf > x[i]) minf = x[i];
  }
  *min = floor(minf/step);
  *max = ceil(maxf/step);
}

/* Computes the z-component of the cross product: (v2 - v1) x (v3 - v1) */
/* This is positive if v3 is to the left of v2-v1 */
inline float is_left(float x1, float y1, float x2, float y2, float x3, float y3) {
  return (((x2-x1)*(y3-y1) - (x3-x1)*(y2-y1)));
}

/* Jarvis' March (avoids sorting points) */
void convex_hull_2d(float x[], float y[], int *n)
{
  int i,sn=1, dir=1;
  float sx[17], sy[17], res;
  float xmin=x[0],ymin=y[0],xmax=x[0],ymax=y[0];
  assert(*n<=16);
  
  //Find lowest point; if tie, choose leftmost; same for highest/rightmost point
  for (i=0; i<*n; i++) {
    if ((y[i]<ymin) || ((y[i]==ymin) && (x[i]<xmin))) {
      ymin = y[i];
      xmin = x[i];
    }
    if ((y[i]>ymax) || ((y[i]==ymax) && (x[i]>xmax))) {
      ymax = y[i];
      xmax = x[i];
    }
  }

  //Check for the corner case that we have a line segment or point:
  for (i=0; i<*n; i++) {
    if ((y[i]!=ymax || x[i] != xmax) && (y[i] != ymin || x[i] != xmin)) break;
  }
  if (i==*n) {
    x[0] = xmax;
    y[0] = ymax;
    *n = 1;
    if (xmax != xmin || ymax != ymin) {
      x[1] = xmin;
      y[1] = ymin;
      *n = 2;
    }
    return;
  }

  sx[0]=xmin;
  sy[0]=ymin;

  //Proceed around the border by finding the next point such that all other
  // points are to the left of the appropriate line
  while (sn<(*n+1) && (sn==1 || sx[sn-1]!=xmin || sy[sn-1]!=ymin)) {
    sx[sn] = x[0];
    sy[sn] = y[0];
    for (i=1; i<*n; i++) {
      if ((x[i]==sx[sn-1]) && (y[i]==sy[sn-1])) continue;
      if ((sx[sn-1]==sx[sn]) && (sy[sn-1]==sy[sn])) {
	sx[sn] = x[i];
	sy[sn] = y[i];
      }
      if (x[i]==sx[sn] && y[i]==sy[sn]) continue;
      res = is_left(sx[sn-1], sy[sn-1], sx[sn], sy[sn], x[i], y[i]);
      if (res > 0) continue;
      if (!res && !((((y[i]-sy[sn])*dir) > 0) || 
		    (y[i]==sy[sn] && (((x[i]-sx[sn])*dir) > 0)))) continue;
      sx[sn] = x[i];
      sy[sn] = y[i];
    }
    if (sx[sn]==xmax && sy[sn]==ymax) { dir = -1; }
    sn++;
  }
  
  if ((sx[sn-1] != sx[0]) || (sy[sn-1] != sy[0])) {
    fprintf(stderr, "Convex hull algorithm failed!\n");
    fprintf(stderr, "Input points:\n");
    for (i=0; i<*n; i++)
      fprintf(stderr, "%.10e %.10e\n", x[i], y[i]);
    exit(1);
  }

  *n = sn;
  memcpy(x, sx, sizeof(float)*sn);
  memcpy(y, sy, sizeof(float)*sn);
}

//Assumes x,y are a convex hull
//Maximum number of inputs: 16
static inline void render_2d(float x[], float y[], int z, int n, float step, 
			     void (*cb)(int, int, int, void*), void *extra) {
  int i,j,nx, j2;
  float yi, yi2, f;
  int min_y, max_y, min_x, max_x;
  float ix[48];

  assert(n<=16);
  min_max(y,n,step,&min_y, &max_y);

  for (i=min_y; i<max_y; i++) {
    yi = i*step;
    yi2 = (i+1)*step;
    nx=0;

    for (j=0; j<n; j++) { //Check intersections w/ lines & points
      j2 = (j+1)%n;
      check_y(yi,j,j2,nx,ix);
      check_y(yi2,j,j2,nx,ix);
      if (y[j]>=yi && y[j] <=yi2) {
	ix[nx] = x[j];
	nx++;
      }
    }

    min_max(ix,nx,step,&min_x,&max_x);
    for (j=min_x; j<max_x; j++) {  //Render 2d
      cb(j, i, z, extra);
    }
  }
}

static inline 
int check_intersections_z(float x[8], float y[8], float z[8], float zi,
			  float ix[], float iy[]) {
  int i, n=0;
  float f;

  for (i=0; i<4; i++) {
    check_z(zi,i,((i+1)%4),n,ix,iy);          //Top face first
    check_z(zi,i,(i+4),n,ix,iy);              //Then sides
    check_z(zi,(i+4),(((i+1)%4)+4),n,ix,iy);  //Then bottom
  }

  return n;
}

//Draws a volumetric trapezoidal prism
//The point order is assumed to be clockwise for the top face
//and then clockwise for the bottom face, in the same order
//such that the sides are formed from lines between points
//0 and 4, 1 and 5, etc.
void trapezoid_prism(float x[8], float y[8], float z[8], float step,
		     void (*cb)(int, int, int, void*), void *extra) {
  float ix[16];
  float iy[16];
  int n=0;
  int i,j;
  float zi = 0, zi2;
  int min_z, max_z;

  min_max(z,8,step,&min_z,&max_z);
  if (!cb) { cb = default_cb; }

  for (i=min_z; i<max_z; i++) {
    zi = i*step;
    zi2 = (i+1)*step;
    n = check_intersections_z(x,y,z,zi,ix,iy);
    n += check_intersections_z(x,y,z,zi2,ix+n,iy+n);
    for (j=0; j<8; j++) { //Check points
      if (z[j]>=zi && z[j] <=zi2) {
	ix[n] = x[j];
	iy[n] = y[j];
	n++;
      }
    }
    convex_hull_2d(ix, iy, &n);
    render_2d(ix, iy, i, n, step, cb, extra);
  }
}

/*
int main(void) {
  float x[8] = {0.05, 0.35, 0.15, -0.15, 10.05, 12.05, 11.45, 9.45};
  float y[8] = {-0.5, 0.5, 0.35, -0.35, -5, 5, 3.7, -3.7};
  float z[8] = {0, 0.1, 1.23, 1.12, 10, 11.43, 15.02, 14.06};
  trapezoid_prism(x,y,z,0.1,NULL,NULL);
  return 0;
}
*/
