#ifndef _RANK_HALO_SPLINES_H_
#define _RANK_HALO_SPLINES_H_


#define NUM_LABELS 3  /* Total number of columns*/
#define VMP 0
#define DLOGVMAX 1
#define TIDAL_FORCE 2

#define SCALE_NAME "/scale"
#define VMP_NAME "/Vmax_Mpeak"
#define DLOGVMAX_NAME "/Log_Vmax_Vmax_max_Tdyn_Tmpeak_"
#define TIDAL_FORCE_NAME "/Log_Vmax_Vmax_max_Tdyn_Tmpeak_"

#define NUM_PROPS (NUM_LABELS)  /* Don't change */

struct halo_property {  /* Don't change */
  char label[50];
  int important;
  int scaling;
};


/*Column descriptors: name, calculate(1)/don't calculate(0) splines, and expected vmax scaling power */

struct halo_property properties[NUM_PROPS] =
  /* Less likely to change */
  {{"Vmax_Mpeak",0,0}, {"DeltaLogVmax",1,0}, {"Tidal_Force_Tdyn",1,0}};

#endif /* _RANK_HALO_SPLINES_H_ */


