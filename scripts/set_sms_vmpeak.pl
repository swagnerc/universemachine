#!/usr/bin/perl -w
use Math::Random qw(random_normal);

my @scatters = (0,0.1,0.2,0.3,0.4,0.5,0.6);
my @iterations = (1, 5, 5, 7, 9, 10);

#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM
my $c = 0;
while (<>) {
    my @a = split;
    @a = @a[0..16];
    if (/^#/) {
	next if ($c);
	print "#Redshift: 0\n";
	print "#Simulation: Bolshoi-Planck\n";
	print "#Cosmology: Om=0.307, Ol=0.693, h=0.6777\n";
	print "#ID: Halo ID\n";
	print "#DescID: Halo descendant ID (-1 for all halos at z=0)\n";
	print "#UPID: ID of parent halo (if any)\n";
	print "#Flags: Ignore for now.\n";
	print "#Uparent_Dist: Distance to parent halo / parent halo's virial radius.\n";
	print "#XYZ: Halo position (comoving Mpc/h)\n";
	print "#VX,VY,VZ: Halo velocity (peculiar physical km/s)\n";
	print "#M: Present-day halo mass (Msun/h)\n";
	print "#V: Present-day Vmax (km/s)\n";
	print "#MP: Peak historical halo mass (Msun/h)\n";
	print "#VMP: Peak historical Vmax (km/s)\n";
	print "#R: Halo virial radius (kpc/h)\n";
	print "#Rank1: Rank of halo in Delta_Vmax (ignore for now).\n";
	print "#SM_XYZ: Stellar mass, assuming scatter between SM and VMP of XYZ dex.\n";
	print "@a ", join(" ", map { "SM_$_" } @scatters), "\n";
	$c++;
	next;
    }

    my ($vmp) = $a[14]*(1+rand()*0.001);
    next if ($vmp < 60);
    $data{$vmp} = "@a";
    #$mh{$vmp} = $a[13]/0.6777;
    push @vmp, $vmp;
}

open SMS, "<bolshoi_sms.dat";
while (<SMS>) {
    chomp;
    push @sm, $_;
}
close SMS;
@vmp = sort { $b <=> $a } @vmp;
@vmp = @vmp[0..$#sm];

print STDERR "Loaded halos.\n";
@sm = sort {$a <=> $b} @sm;
@vmp = @vmps = sort {$a <=> $b} @vmp;

sub scatter_vmp {
    my ($vmp, $scatter) = @_;
    return 0.2 if ($vmp > 200);
    return $scatter if ($vmp < 120);
    return (0.2+($scatter-0.2)*($vmp-200)/(120-200));
}

for my $s (0..$#scatters) {
    my $scatter = $scatters[$s];
    my %sm;
    for (0..$#sm) {
	my $vsc = scatter_vmp($vmp[$_], $scatter);
	my $sm = $sm[$_]+random_normal(1,0,$vsc);
	$sm{$vmp[$_]} = $sm;
    }
 
    #iterate
    for (0..$iterations[$s]) {
	print STDERR "Iteration $_ (scatter: $scatter dex).\n";
	@vmp = sort { $sm{$a} <=> $sm{$b} } @vmp;
	%sm = map { $vmp[$_], $sm[$_] } (0..$#sm);
	last if ($_ == $iterations[$s]);

	$sum = 0;
	my @tvmp;
	@vmp = sort { $a <=> $b } @vmp;
	for (0..$#vmps) {
	    $sum += $sm{$vmps[$_]};
	    push @tvmp, $sm{$vmps[$_]};
	    if ($_ >= 500) { $sum -= shift @tvmp; }
	    my $av = $sum / (($_ >= 500) ? 500 : ($_+1));
	    my $vsc = scatter_vmp($vmps[$_], $scatter);
	    $sm{$vmps[$_]} = $av + random_normal(1,0,$vsc);
	}
    }

    for (@vmp) {
	$data{$_}.= " $sm{$_}";
    }
}

for (@vmp) {
    print "$data{$_}\n";
}

