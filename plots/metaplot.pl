#!/usr/bin/perl -w
my @plots = qw/correl rejuvenation ex_situ sats mp_fraction scatter quenching_thresholds smhm wl obs smfs/;
my @names = ("correlation functions", "rejuvenation", "ex situ fractions", "satellite quenching", "main progenitor histories", "SMHM scatter", "Quenching Thresholds", "smhm", "weak lensing", "observations", "smfs");
my %names = map { $plots[$_] => $names[$_] } (0..$#names);
$ENV{HIGH_QUALITY} = 1;
for (@plots) {
    my $fn = "scripts/plot_$_.pl";
    die "Cannot find/read $fn!\n" unless -r $fn;
    print "Plotting $names{$_}...\n";
    system("perl", $fn, @ARGV);
}

print "Done.\n";
