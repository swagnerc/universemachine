#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/smhm";
our $plotdir = "$dir/graphs/smhm";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0.1, 1..10);
my @scales = get_scales_at_redshifts("$datadir/median_fits", "smhm", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

sub smhm_ratios {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" if ($a[1] == 0);
	return "" if ($a[0] < 10.5);
	return XmGrace::tolin(0..3)->("@a");
    }
}

sub smhm {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" if ($a[1] > -1);
	return "" if ($a[0]+$a[1] < 7);
	return "" if ($a[1]+$a[2] > -0.5);
	return "" if ($z > 0.5 and $a[0]+$a[1] < 8);
	return XmGrace::tolin(0..3)->("@a");
    }
}

sub imf_smhm {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" if ($a[1] > -1);
	return "" if ($a[0]+$a[1] < 7);
#	return "" if ($a[1]+$a[2] > -0.5);
	return "" if ($z > 0.5 and $a[0]+$a[1] < 8);
	my $sm =  $a[0]+$a[1];
	my $vdisp = 2.073+0.293*($sm-10.26)/1.17;
	$vdisp = 2.073+0.403*($sm-10.26) if ($sm < 10.26);
	my $loga = 0.38*($vdisp - log(200)/log(10))-0.06;
	$loga += log(1.7)/log(10);
	$loga = log(0.25 + 0.75*(10**$loga))/log(10);
	$a[1] += $loga;
	return XmGrace::tolin(0..1)->("@a");
    }
}

sub smhm_sfq {
    my $z = shift;
    return sub {
	my @a = split(" ", $_[0]);
	return "" if ($a[2] < 0 or $a[1] > -1 or $a[1]+$a[2] > -1);
	return "" if ($a[0]+$a[1] < 7);
	return "" if ($z > 0.5 and $a[0]+$a[1] < 8);
	return XmGrace::tolin(0..3)->("@a");
    }
}

sub smhm_sfq_ratios {
    my ($z, $t) = @_;
    return sub {
	my @a = split(" ", $_[0]);
	return "" if ($a[1] == 0 or $a[2]==0);
	return "" if ($a[0] < 10.5);
	return "" if ($z > 0.5 and $t eq 'q' and $a[0] < 11.5);
	return XmGrace::tolin(0..3)->("@a");
    }
}



sub smhm_sm {
    my $z = shift;
    my $sub = smhm($z);
    return sub {
	my @a = split(" ", $sub->($_[0]));
	return "" unless (@a);
	$_*=$a[0] for (@a[1..3]);
	return "@a";
    }
}

my $fn = sprintf("smhm_true");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $smhm_ratio_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 0.0001, 0.1),
	    rainbow => 1,
	    legendxy => "0.5, 0.42",
	    legend2xy => "0.8, 0.427",
	    legend2vg => "0.0483",
	    data => [(map {{data => "file[0,22,23,24]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), ($zs[$_] < 6) ? 'legend' : 'legend2', "z = $zs[$_]", type => "xydydy", sskip => 1}} (0..$#zs))])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("smhm_imf");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $smhm_ratio_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 0.0001, 0.1),
	    rainbow => 1,
	    legendxy => "0.5, 0.42",
	    legend2xy => "0.8, 0.427",
	    legend2vg => "0.0483",
	    data => [{data => "file[0,1,23,24]:$datadir/median_fits/smhm_a$scales[0].dat", transform => smhm($zs[0]), 'legend' => "Universal IMF", type => "xydydy", sskip => 1},
		     {data => "file[0,1]:$datadir/median_fits/smhm_a$scales[0].dat", transform => imf_smhm($zs[0]), 'legend' => "Mass-Dependent IMF", type => "xy", sskip => 1},])->write_pdf("$plotdir/AGR/$fn.agr");


$fn = sprintf("smhm_true_sm");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $sm_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 1e7, 1e12),
	    rainbow => 1,
	    legendxy => "0.65, 0.47",
	    legend2xy => "0.95, 0.477",
	    legend2vg => "0.0483",
	    data => [(map {{data => "file[0,22,23,24]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm_sm($zs[$_]), ($zs[$_] < 6) ? 'legend' : 'legend2', "z = $zs[$_]", type => "xydydy", sskip => 1}} (0..$#zs))])->write_pdf("$plotdir/AGR/$fn.agr");


@zs = (0, 1, 2);
@scales = get_scales_at_redshifts("$datadir/median_fits", "smhm", @zs);
%zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales_ratios.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

$fn = sprintf("smhm_cen_sat");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $smhm_ratio_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 0.0001, 0.1),
	    fading_colors => 1,
	    text => [{text => "Centrals", pos => "0.56, 0.35"},
		     {text => "Satellites", pos => "0.86, 0.35"},],
	    legendxy => "0.53, 0.35",
	    legend2xy => "0.83, 0.357",
	    legend2vg => "0.043",
	    data => [#(map {{data => "file[0,25,26,27]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,34,35,36]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
		     (map {{data => "file[0,25,26,27]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend => "z = $zs[$_]", lcolor => 3+6*($_), sskip => 1}} (0..$#zs)),
		     (map {{data => "file[0,34,35,36]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend2 => "z = $zs[$_]", lcolor => 2+6*($_), sskip => 1}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("smhm_cen_ratio");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => 'M\s*,cen|sat\N / M\s*,all',
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0.5, 2, 0.2, 1),
	    fading_colors => 1,
	    text => [{text => "Centrals", pos => "0.63, 0.76"},
		     {text => "Satellites", pos => "0.93, 0.76"},],
	    legendxy => "0.6, 0.76",
	    legend2xy => "0.9, 0.767",
	    legend2vg => "0.043",
	    data => [#(map {{data => "file[0,1,2,3]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,4,5,6]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
		     {data => "1e10 1\n1e15 1", lcolor => 1, lwidth => 2.5, lstyle => "2"},
		     (map {{data => "file[0,1,2,3]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), legend => "z = $zs[$_]", lcolor => 3+6*($_), type => "xydydy", sskip => 1}} (0..$#zs)),
		     (map {{data => "file[0,4,5,6]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), legend2 => "z = $zs[$_]", lcolor => 2+6*($_), type => "xydydy", sskip => 1}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");




$fn = sprintf("smhm_sf_q");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $smhm_ratio_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 0.0001, 0.1),
	    fading_colors => 1,
	    text => [{text => "Star-Forming", pos => "0.52, 0.35"},
		     {text => "Quenched", pos => "0.84, 0.35"},],
	    legendxy => "0.53, 0.35",
	    legend2xy => "0.83, 0.357",
	    legend2vg => "0.043",
	    data => [
#(map {{data => "file[0,31,32,33]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,34,35,36]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
#		     (map {{data => "file[0,31,32,33]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend => "z = $zs[$_]", lcolor => 3+6*($_), sskip => 1}} (0..$#zs)),
#		     (map {{data => "file[0,34,35,36]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend2 => "z = $zs[$_]", lcolor => 2+6*($_), sskip => 1}} (0..$#zs))
		     (map {{data => "file[0,37,38,39]:$datadir/median_raw/smhm_a$scales[$_].dat", transform => smhm_sfq($zs[$_]), type => "xydydy", legend => "z = $zs[$_]", lcolor => 3+6*($_), sskip => 1}} (0..$#zs)),
		     (map {{data => "file[0,40,41,42]:$datadir/median_raw/smhm_a$scales[$_].dat", transform => smhm_sfq($zs[$_]), type => "xydydy", legend2 => "z = $zs[$_]", lcolor => 2+6*($_), sskip => 1}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");



$fn = sprintf("smhm_sf_q_fits");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => $smhm_ratio_label,
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::logscale('y', 0.0001, 0.1),
	    fading_colors => 1,
	    text => [{text => "Star-Forming", pos => "0.52, 0.35"},
		     {text => "Quenched", pos => "0.84, 0.35"},],
	    legendxy => "0.53, 0.35",
	    legend2xy => "0.83, 0.357",
	    legend2vg => "0.043",
	    data => [
#(map {{data => "file[0,31,32,33]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,34,35,36]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
		     (map {{data => "file[0,37,38,39]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend => "z = $zs[$_]", lcolor => 3+6*($_), sskip => 1}} (0..$#zs)),
		     (map {{data => "file[0,40,41,42]:$datadir/median_fits/smhm_a$scales[$_].dat", transform => smhm($zs[$_]), type => "xydydy", legend2 => "z = $zs[$_]", lcolor => 2+6*($_), sskip => 1}} (0..$#zs))
#		     (map {{data => "file[0,31,32,33]:$datadir/median_raw/smhm_a$scales[$_].dat", transform => smhm_sfq($zs[$_]), type => "xydydy", legend => "z = $zs[$_]", lcolor => 3+6*($_), sskip => 1}} (0..$#zs)),
#		     (map {{data => "file[0,34,35,36]:$datadir/median_raw/smhm_a$scales[$_].dat", transform => smhm_sfq($zs[$_]), type => "xydydy", legend2 => "z = $zs[$_]", lcolor => 2+6*($_), sskip => 1}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("smhm_sf_ratio");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => 'M\s*,SF|Q\N / M\s*,all',
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0.5, 2, 0.2, 1),
	    fading_colors => 1,
	    text => [{text => "Star-Forming", pos => "0.59, 0.76"},
		     {text => "Quenched", pos => "0.91, 0.76"},],
	    legendxy => "0.6, 0.76",
	    legend2xy => "0.9, 0.767",
	    legend2vg => "0.043",
	    data => [{data => "1e10 1\n1e15 1", lcolor => 1, lwidth => 2.5, lstyle => 2},#(map {{data => "file[0,7,8,9]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,10,11,12]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
		     (map {{data => "file[0,7,8,9]:$datadir/median_raw/ratios_a$scales[$_].dat", transform => smhm_sfq_ratios($zs[$_], 's'), legend => "z = $zs[$_]", type => "xydydy", sskip => 2, smooth => 1, lcolor => 3+6*($_)}} (0..$#zs)),
		     (map {{data => "file[0,10,11,12]:$datadir/median_raw/ratios_a$scales[$_].dat", transform => smhm_sfq_ratios($zs[$_], 'q'), legend2 => "z = $zs[$_]", type => "xydydy", sskip => 2, smooth => 1, lcolor => 2+6*($_)}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");


$fn = sprintf("smhm_sf_fits_ratio");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => 'M\s*,SF|Q\N / M\s*,all',
	    XmGrace::logscale('x', 1e10, 1e15),
	    XmGrace::linscale('y', 0.5, 2, 0.2, 1),
	    fading_colors => 1,
	    text => [{text => "Star-Forming", pos => "0.59, 0.76"},
		     {text => "Quenched", pos => "0.91, 0.76"},],
	    legendxy => "0.6, 0.76",
	    legend2xy => "0.9, 0.767",
	    legend2vg => "0.043",
	    data => [{data => "1e10 1\n1e15 1", lcolor => 1, lwidth => 2.5, lstyle => 2},#(map {{data => "file[0,7,8,9]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 3+6*($_+1)}} (0)),
		     #(map {{data => "file[0,10,11,12]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_ratios($zs[$_]), metatype => "errorstrip", fcolor => 2+6*($_+1)}} (0)),
		     (map {{data => "file[0,7,8,9]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_sfq_ratios($zs[$_], 's'), legend => "z = $zs[$_]", type => "xydydy", sskip => 2, smooth => 1, lcolor => 3+6*($_)}} (0..$#zs)),
		     (map {{data => "file[0,10,11,12]:$datadir/median_fits/ratios_a$scales[$_].dat", transform => smhm_sfq_ratios($zs[$_], 'q'), legend2 => "z = $zs[$_]", type => "xydydy", sskip => 2, smooth => 1, lcolor => 2+6*($_)}} (0..$#zs))
		     
])->write_pdf("$plotdir/AGR/$fn.agr");


MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/smhm_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}
