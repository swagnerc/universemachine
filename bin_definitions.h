#ifndef _BIN_DEFINITIONS_H_
#define _BIN_DEFINITIONS_H_

#define SM_MIN 5
#define SM_BPDEX 20
#define SM_NBINS 162
#define SM_SMOOTH_EXTRA_DEX 3
#define SM_SMOOTH_BPDEX 100
#define SM_SMOOTH_MIN (SM_MIN-(double)SM_SMOOTH_EXTRA_DEX)
#define SM_SMOOTH_NBINS ((int)(SM_NBINS*SM_SMOOTH_BPDEX/SM_BPDEX)+SM_SMOOTH_EXTRA_DEX*SM_SMOOTH_BPDEX)
#define SM_MAX (SM_MIN + (double)(SM_NBINS-2)/(double)SM_BPDEX)
#define SM_RED_BPDEX 5
#define SM_RED_NBINS 41

#define SMHM_NUM_TYPES 16
#define SMHM_SMHM_BPDEX 100
#define SMHM_SMHM_MIN (-4.5)
#define SMHM_SMHM_NBINS (4*SMHM_SMHM_BPDEX)
#define SMHM_FIT_BPDEX 5
#define SMHM_FIT_NBINS (7*SMHM_FIT_BPDEX)
#define SMHM_HM_BPDEX 20
#define SMHM_HM_MIN 9
#define SMHM_HM_NBINS (7*SMHM_HM_BPDEX)
#define SMHM_NBINS (SMHM_SMHM_NBINS*SMHM_HM_NBINS)
#define SMHM_NUM_ZS 27
#define SMHM_ZS {0, 0.1, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 11, 12, 13, 14, 15}
#include "fit_smhm.h"

#define WL_BPDEX 8
#define WL_MIN_R (-2)
#define NUM_WL_THRESHES 3
#define NUM_WL_TYPES 3
#define NUM_WL_BINS 24
#define NUM_WL_ZS   5
#define WL_ZS {0, 0.1, 0.5, 1, 2}

#define SSFR_SSFR_MIN    (-14)
#define SSFR_SSFR_BPDEX  20
#define SSFR_SSFR_NBINS (7*SSFR_SSFR_BPDEX)
#define SSFR_SM_MIN      7
#define SSFR_SM_BPDEX    SM_RED_BPDEX
#define SSFR_SM_NBINS    SM_RED_NBINS
#define SSFR_NBINS       (SSFR_SM_NBINS*SSFR_SSFR_NBINS)

#define TS_MIN 0
#define TS_MAX 14
#define TS_BPGYR 20
#define TS_NBINS (TS_BPGYR*(TS_MAX-TS_MIN+1))

#define HM_M_MIN 7.875
#define HM_M_BPDEX 4
#define HM_M_NBINS (8*HM_M_BPDEX)

#define UV_MIN (-24)
#define UV_MAX (-14)
#define UV_BPMAG 20
#define UV_NBINS ((int)(UV_BPMAG*(UV_MAX-UV_MIN)+2))
#define UV_SMOOTH_EXTRA_MAG 2
#define UV_SMOOTH_BPMAG 100
#define UV_SMOOTH_MAX (UV_MAX + UV_SMOOTH_EXTRA_MAG)
#define UV_SMOOTH_NBINS ((int)(UV_SMOOTH_BPMAG*(UV_SMOOTH_MAX-UV_MIN)+2))

#define UV_SFR_MIN (-24)
#define UV_SFR_MAX (-14)
#define UV_SFR_BPMAG 5
#define UV_SFR_NBINS ((int)(UV_SFR_BPMAG*(UV_SFR_MAX-UV_SFR_MIN)+2))

#define UVSM_REDSHIFTS 16
#define UVSM_MAX (-17)
#define UVSM_MIN (-24)
#define UVSM_BPMAG 2
#define UVSM_UV_BINS (UVSM_BPMAG*(UVSM_MAX-UVSM_MIN))
#define UVSM_SM_BINS 120
#define UVSM_SM_BPDEX 50
#define UVSM_SM_NORM 8.5
#define UVSM_SM_NORM_M0 (-21)
#define UVSM_SM_SLOPE (-0.4)

#define MIN_DIST (-1)
#define MAX_DIST 1
#define DIST_BPDEX 5
#define NUM_BINS (((MAX_DIST-MIN_DIST) * DIST_BPDEX))
#define INV_DIST_BPDEX (1.0/(double)DIST_BPDEX)

#define MIN_CONF_DIST_NOH (-2)
#define MAX_CONF_DIST_NOH 1
#define DIST_CONF_BPDEX 2
#define NUM_CONF_BINS (((MAX_CONF_DIST_NOH-MIN_CONF_DIST_NOH) * DIST_CONF_BPDEX)+1)
#define INV_DIST_CONF_BPDEX (1.0/(double)DIST_CONF_BPDEX)
#define MIN_CONF_DIST (MIN_CONF_DIST_NOH-0.167491087293764)
#define MAX_CONF_DIST (MAX_CONF_DIST_NOH-0.167491087293764)

#define NUM_CORR_TYPES 4
#define NUM_CORR_THRESHES 4
#define NUM_CORRS_PER_STEP (NUM_CORR_THRESHES*NUM_CORR_TYPES)
#define NUM_CORR_BINS_PER_STEP (NUM_CORRS_PER_STEP*NUM_BINS)

#define NUM_XCORR_THRESHES 1
#define NUM_XCORRS_PER_STEP (NUM_XCORR_THRESHES*NUM_CORR_TYPES)
#define NUM_XCORR_BINS_PER_STEP (NUM_XCORRS_PER_STEP*NUM_BINS)

static const int64_t XCORR_BIN1[NUM_XCORR_THRESHES] = {3};
static const int64_t XCORR_BIN2[NUM_XCORR_THRESHES] = {1};
//Cross-correlation types: AllxAll, AllxQ, QxQ, SFxSF
static const int64_t XCORR_COUNTS1[4] = {0,0,2,1}; //All, All, Q, SF
static const int64_t XCORR_COUNTS2[4] = {0,2,2,1}; //All,   Q, Q, SF


#define NUM_CONF_BINS_PER_STEP (6*NUM_CONF_BINS)

#define NUM_DENS_BINS (10)


extern double max_dist, min_dist;
extern float corr_dists[NUM_BINS+1];

extern double max_conf_dist, min_conf_dist;
extern float conf_dists[NUM_CONF_BINS+1];

void init_dists(void);
void init_conf_dists(void);
float moustakas_translate_const(float scale);

#define MAX_BINS_NN 101

inline int64_t dens_bin_edge(int64_t b) {
  if (b < 3) return b;
  if (b == 3) return 3;
  if (b == 4) return 6;
  if (b == 5) return 10;
  if (b == 6) return 18;
  if (b == 7) return 32;
  if (b == 8) return 56;
  if (b == 9) return 100;
  return 151;
}

inline int64_t dens_bin_of(int64_t nn) {
  if (nn < 3) return nn;
  if (nn < 6) return 3;
  if (nn < 10) return 4;
  if (nn < 18) return 5;
  if (nn < 32) return 6;
  if (nn < 56) return 7;
  if (nn < 100) return 8;
  return 9;
}


//Brute-force search is best, given that there are only 10 bins.
inline int64_t corr_bin_of(float dist) {
  int64_t i;
  for (i=NUM_BINS; i>=0; i--) if (dist > corr_dists[i]) break;
  return i;
}


inline int64_t conf_bin_of(float dist) {
  int64_t i;
  for (i=NUM_CONF_BINS; i>=0; i--) if (dist > conf_dists[i]) break;
  return i;
}

#endif /* _BIN_DEFINITIONS_H_ */
