#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <sys/mman.h>
#include "sf_model.h"
#include "make_sf_catalog.h"
#include "check_syscalls.h"
#include "stringparse.h"
#include "inthash.h"

struct inthash *idx = NULL;
struct wl_data *sh = NULL;
int64_t num_sh = 0;

int main(int argc, char **argv) {
  if (argc < 5) {
    fprintf(stderr, "Usage: %s shear_catalog scale halo_catalogs_path num_blocks...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  double a = atof(argv[2]);

  char *INBASE = argv[3];
  int64_t num_blocks = atol(argv[4]);
  
  char buffer[1024];
  FILE *in = check_fopen(argv[1], "r");
  struct wl_data s;
  int64_t i, j, n, snap;
  void *data[NUM_WL_BINS+4];
  enum parsetype pt[NUM_WL_BINS+4];

  int64_t num_scales=0, offset, *offsets = NULL;
  float scale, *scales = NULL;
  
  data[0] = &s.id;
  pt[0] = PARSE_INT64;
  data[1] = data[2] = data[3] = NULL;
  pt[1] = pt[2] = pt[3] = PARSE_SKIP;
  for (i=0; i<NUM_WL_BINS; i++) {
    data[i+4] = s.pcounts+i;
    pt[i+4] = PARSE_INT32;
  }

  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    if (stringparse(buffer, data, pt, NUM_WL_BINS+4)!=(NUM_WL_BINS+4)) continue;
    check_realloc_every(sh, sizeof(struct wl_data), num_sh, 1000);
    sh[num_sh] = s;
    num_sh++;
  }
  fclose(in);

  struct inthash *idx = new_inthash();
  ih_prealloc(idx, num_sh);
  for (i=0; i<num_sh; i++) ih_setval(idx, sh[i].id, sh+i);

  for (i=0; i<num_blocks; i++) {
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, i);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);

    for (j=0; j<num_scales; j++)
      if (fabs(scales[j]-a) < 1e-5) break;

    if (j==num_scales) {
      fprintf(stderr, "[Error] Could not find scale %f with tolerance 1e-5.\n", a);
      exit(EXIT_FAILURE);
    }
    
    int64_t length = 0, k;
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, i);
    struct catalog_halo *ch = check_mmap_file(buffer, 'r', &length);
    snprintf(buffer, 1024, "%s/pcounts_%f.%"PRId64".bin", INBASE, scales[j], i);
    FILE *out = check_fopen(buffer, "w");
    
    for (k=offsets[j]; k<offsets[j+1]; k++) {
      struct wl_data *s = ih_getval(idx, ch[k].id);
      assert(s);
      fwrite(s, sizeof(struct wl_data), 1, out);
    }
    fclose(out);
    
    munmap(ch, length);
    num_scales = 0;
  }
  return 0;
}
